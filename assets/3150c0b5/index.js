$j(function() {
  // $j(document).ready(function(){
      var page = 1;
      $j('#next_page').on('click', function(){
      //$j('#next_page').css('display', 'none');
      $j('#wait_img').css('display', 'block');
        $j.ajax({
            url: '<?php echo Yii::app()->request->baseUrl; ?>/index.php/cabinet/ajax/user/<?php echo $user_id; ?>/page/'+page,
            type: 'GET',
            data: {'id': 21},
            complete: function(data){
               //console.log(data.responseText);
               $j('#wait_img').css('display', 'none');
               //$j('#next_page').css('display', '');
               $j('#product_preference_list').append(data.responseText);
               page++;
            }
        }); 
      });

      $j('#select_all_categories').on('click', function(){
        var dat = {'all': 'all'};
            if($j(this).attr('checked') == 'checked')
            $j('.check_cat').each(function(){
               $j(this).attr('checked', 'checked');
            });
            if($j(this).attr('checked') != 'checked'){
              dat = {'all': 'none'};
              $j('.check_cat').each(function(){
               $j(this).removeAttr('checked');
              });
            }
            
      //select all categories
       $j.ajax({
            url: '<?php echo Yii::app()->request->baseUrl; ?>/index.php/cabinet/ajax/user/<?php echo $user_id; ?>/',
            type: 'GET',
            data: dat,
            complete: function(data){
             //console.log(data.responseText);
             $j('#product_preference_list').html($j('#wait_img').html());
             $j('#product_preference_list').html(data.responseText);
           // console.log('ajax');
          }
        });
      });
/************************
      var isLoading = false;

      $j(window).scroll(
        function (){
            if(isLoading) return false;
              var scrollable = document.getElementById("product_preference_list");
              var endPos = $j(window).height() - (scrollable.clientHeight - $j(window).scrollTop());
              //console.log(endPos);
            if(endPos > 300){//window.screen.height
              isLoading = true;

                $j('#next_page').css('display', 'none');
                $j('#wait_img').css('display', 'block');

                $j.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/index.php/cabinet/ajax/user/<?php echo $user_id; ?>/page/'+page,
                    type: 'GET',
                    data: {'id': 21},
                    complete: function(data){
                       //console.log(data.responseText);
                       $j('#wait_img').css('display', 'none');
                       $j('#next_page').css('display', '');
                       $j('#product_preference_list').append(data.responseText);
                       page++;
                       isLoading = false;
                    }
                });

          }
      });
********/
});