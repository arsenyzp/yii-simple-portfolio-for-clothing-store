<script type="text/javascript">
	$j(function() {
      $j('.check_cat').on('click', function(){
      	console.log('click!');
      	$j('#product_preference_list').html($j('#wait_img').html());
        $j.ajax({
          url: ajax_url,
          type: 'GET',
          data: {'status': ($j(this).attr('checked') == 'checked') ? $j(this).val() : 0, 'id_cat': $j(this).val()},
          complete: function(data){
             //console.log(data.responseText);
             $j('#product_preference_list').html(data.responseText);
            console.log('ajax');
          }
        });
      });

      $j('.sale_check').on('click', function(){
      	console.log('click!');
      	$j('#product_preference_list').html($j('#wait_img').html());
        $j.ajax({
          url: ajax_url,
          type: 'GET',
          data: {'status': ($j(this).attr('checked') == 'checked') ? $j(this).val() : 0, 'sale': $j(this).val()},
          complete: function(data){
             //console.log(data.responseText);
             $j('#product_preference_list').html(data.responseText);
            console.log('ajax');
          }
        });
      });

    $j('#your_brands').on('click', function(){
    	$j('#your_brands_list').toggle(500);
    });
    $j('#your_size').on('click', function(){
    	$j('#your_size_list').toggle(500);
    });
   });
</script>