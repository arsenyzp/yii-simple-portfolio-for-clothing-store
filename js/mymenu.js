var $j = jQuery.noConflict();

(function ($j) {

    $j(document).ready(function () {
        $j('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();

       $j('#close_pop').click(function() {
       	
            $j('#popup_name').css('display', 'none');
            $j('#fade').css('display', 'none');
        });
		$j('#close_pop_error').click(function() {
		  $j('#popup_name_erorr').css('display', 'none');
		  $j('#fade').css('display', 'none');
		 
		});
        

    });

    function format(str) {
        for (var i = 1; i < arguments.length; i++) {
            str = str.replace('%' + (i - 1), arguments[i]);
        }
        return str;
    }

    function CloudZoom(jWin, opts) {
        var sImg = $j('img', jWin);
		var	img1;
		var	img2;
        var zoomDiv = null;
		var	$jmouseTrap = null;
		var	lens = null;
		var	$jtint = null;
		var	softFocus = null;
		var	$jie6Fix = null;
		var	zoomImage;
        var controlTimer = 0;      
        var cw, ch;
        var destU = 0;
		var	destV = 0;
        var currV = 0;
        var currU = 0;      
        var filesLoaded = 0;
        var mx,
            my; 
        var ctx = this, zw;
        // Display an image loading message. This message gets deleted when the images have loaded and the zoom init function is called.
        // We add a small delay before the message is displayed to avoid the message flicking on then off again virtually immediately if the
        // images load really fast, e.g. from the cache. 
        //var	ctx = this;
        setTimeout(function () {
            //						 <img src="/images/loading.gif"/>
            if ($jmouseTrap === null) {
                var w = jWin.width();
                jWin.parent().append(format('<div style="width:%0px;position:absolute;top:75%;left:%1px;text-align:center" class="cloud-zoom-loading" >Loading...</div>', w / 3, (w / 2) - (w / 6))).find(':last').css('opacity', 0.5);
            }
        }, 200);


        var ie6FixRemove = function () {

            if ($jie6Fix !== null) {
                $jie6Fix.remove();
                $jie6Fix = null;
            }
        };

        // Removes cursor, tint layer, blur layer etc.
        this.removeBits = function () {
            //$jmouseTrap.unbind();
            if (lens) {
                lens.remove();
                lens = null;             
            }
            if ($jtint) {
                $jtint.remove();
                $jtint = null;
            }
            if (softFocus) {
                softFocus.remove();
                softFocus = null;
            }
            ie6FixRemove();

            $j('.cloud-zoom-loading', jWin.parent()).remove();
        };


        this.destroy = function () {
            jWin.data('zoom', null);

            if ($jmouseTrap) {
                $jmouseTrap.unbind();
                $jmouseTrap.remove();
                $jmouseTrap = null;
            }
            if (zoomDiv) {
                zoomDiv.remove();
                zoomDiv = null;
            }
            //ie6FixRemove();
            this.removeBits();
            // DON'T FORGET TO REMOVE JQUERY 'DATA' VALUES
        };


        // This is called when the zoom window has faded out so it can be removed.
        this.fadedOut = function () {
            
			if (zoomDiv) {
                zoomDiv.remove();
                zoomDiv = null;
            }
			 this.removeBits();
            //ie6FixRemove();
        };

        this.controlLoop = function () {
            if (lens) {
                var x = (mx - sImg.offset().left - (cw * 0.5)) >> 0;
                var y = (my - sImg.offset().top - (ch * 0.5)) >> 0;
               
                if (x < 0) {
                    x = 0;
                }
                else if (x > (sImg.outerWidth() - cw)) {
                    x = (sImg.outerWidth() - cw);
                }
                if (y < 0) {
                    y = 0;
                }
                else if (y > (sImg.outerHeight() - ch)) {
                    y = (sImg.outerHeight() - ch);
                }

                lens.css({
                    left: x,
                    top: y
                });
                lens.css('background-position', (-x) + 'px ' + (-y) + 'px');

                destU = (((x) / sImg.outerWidth()) * zoomImage.width) >> 0;
                destV = (((y) / sImg.outerHeight()) * zoomImage.height) >> 0;
                currU += (destU - currU) / opts.smoothMove;
                currV += (destV - currV) / opts.smoothMove;

                zoomDiv.css('background-position', (-(currU >> 0) + 'px ') + (-(currV >> 0) + 'px'));              
            }
            controlTimer = setTimeout(function () {
                ctx.controlLoop();
            }, 30);
        };

        this.init2 = function (img, id) {

            filesLoaded++;
            //console.log(img.src + ' ' + id + ' ' + img.width);	
            if (id === 1) {
                zoomImage = img;
            }
            //this.images[id] = img;
            if (filesLoaded === 2) {
                this.init();
            }
        };

        /* Init function start.  */
        this.init = function () {
            // Remove loading message (if present);
            $j('.cloud-zoom-loading', jWin.parent()).remove();


/* Add a box (mouseTrap) over the small image to trap mouse events.
		It has priority over zoom window to avoid issues with inner zoom.
		We need the dummy background image as IE does not trap mouse events on
		transparent parts of a div.
		*/
            $jmouseTrap = jWin.parent().append(format("<div class='mousetrap' style='background-image:url(\".\");z-index:999;position:absolute;width:%0px;height:%1px;left:%2px;top:%3px;\'></div>", sImg.outerWidth(), sImg.outerHeight(), 0, 0)).find(':last');

            //////////////////////////////////////////////////////////////////////			
            /* Do as little as possible in mousemove event to prevent slowdown. */
            $jmouseTrap.bind('mousemove', this, function (event) {
                // Just update the mouse position
                mx = event.pageX;
                my = event.pageY;
            });
            //////////////////////////////////////////////////////////////////////					
            $jmouseTrap.bind('mouseleave', this, function (event) {
                clearTimeout(controlTimer);
                //event.data.removeBits();                
				if(lens) { lens.fadeOut(299); }
				if($jtint) { $jtint.fadeOut(299); }
				if(softFocus) { softFocus.fadeOut(299); }
				zoomDiv.fadeOut(300, function () {
                    ctx.fadedOut();
                });																
                return false;
            });
            //////////////////////////////////////////////////////////////////////			
            $jmouseTrap.bind('mouseenter', this, function (event) {
				mx = event.pageX;
                my = event.pageY;
                zw = event.data;
                if (zoomDiv) {
                    zoomDiv.stop(true, false);
                    zoomDiv.remove();
                }

                var xPos = opts.adjustX,
                    yPos = opts.adjustY;
                             
                var siw = sImg.outerWidth();
                var sih = sImg.outerHeight();

                var w = opts.zoomWidth;
                var h = opts.zoomHeight;
                if (opts.zoomWidth == 'auto') {
                    w = siw;
                }
                if (opts.zoomHeight == 'auto') {
                    h = sih;
                }
                //$j('#info').text( xPos + ' ' + yPos + ' ' + siw + ' ' + sih );
                var appendTo = jWin.parent(); // attach to the wrapper			
                switch (opts.position) {
                case 'top':
                    yPos -= h; // + opts.adjustY;
                    break;
                case 'right':
                    xPos += siw; // + opts.adjustX;					
                    break;
                case 'bottom':
                    yPos += sih; // + opts.adjustY;
                    break;
                case 'left':
                    xPos -= w; // + opts.adjustX;					
                    break;
                case 'inside':
                    w = siw;
                    h = sih;
                    break;
                    // All other values, try and find an id in the dom to attach to.
                default:
                    appendTo = $j('#' + opts.position);
                    // If dom element doesn't exit, just use 'right' position as default.
                    if (!appendTo.length) {
                        appendTo = jWin;
                        xPos += siw; //+ opts.adjustX;
                        yPos += sih; // + opts.adjustY;	
                    } else {
                        w = appendTo.innerWidth();
                        h = appendTo.innerHeight();
                    }
                }

                zoomDiv = appendTo.append(format('<div id="cloud-zoom-big" class="cloud-zoom-big" style="display:none;position:absolute;left:%0px;top:%1px;width:%2px;height:%3px;background-image:url(\'%4\');z-index:99;"></div>', xPos, yPos, w, h, zoomImage.src)).find(':last');

                // Add the title from title tag.
                if (sImg.attr('title') && opts.showTitle) {
                    zoomDiv.append(format('<div class="cloud-zoom-title">%0</div>', sImg.attr('title'))).find(':last').css('opacity', opts.titleOpacity);
                }

                // Fix ie6 select elements wrong z-index bug. Placing an iFrame over the select element solves the issue...		
                if ($j.browser.msie && $j.browser.version < 7) {
                    $jie6Fix = $j('<iframe frameborder="0" src="#"></iframe>').css({
                        position: "absolute",
                        left: xPos,
                        top: yPos,
                        zIndex: 99,
                        width: w,
                        height: h
                    }).insertBefore(zoomDiv);
                }

                zoomDiv.fadeIn(500);

                if (lens) {
                    lens.remove();
                    lens = null;
                } /* Work out size of cursor */
                cw = (sImg.outerWidth() / zoomImage.width) * zoomDiv.width();
                ch = (sImg.outerHeight() / zoomImage.height) * zoomDiv.height();

                // Attach mouse, initially invisible to prevent first frame glitch
                lens = jWin.append(format("<div class = 'cloud-zoom-lens' style='display:none;z-index:98;position:absolute;width:%0px;height:%1px;'></div>", cw, ch)).find(':last');

                $jmouseTrap.css('cursor', lens.css('cursor'));

                var noTrans = false;

                // Init tint layer if needed. (Not relevant if using inside mode)			
                if (opts.tint) {
                    lens.css('background', 'url("' + sImg.attr('src') + '")');
                    $jtint = jWin.append(format('<div style="display:none;position:absolute; left:0px; top:0px; width:%0px; height:%1px; background-color:%2;" />', sImg.outerWidth(), sImg.outerHeight(), opts.tint)).find(':last');
                    $jtint.css('opacity', opts.tintOpacity);                    
					noTrans = true;
					$jtint.fadeIn(500);

                }
                if (opts.softFocus) {
                    lens.css('background', 'url("' + sImg.attr('src') + '")');
                    softFocus = jWin.append(format('<div style="position:absolute;display:none;top:2px; left:2px; width:%0px; height:%1px;" />', sImg.outerWidth() - 2, sImg.outerHeight() - 2, opts.tint)).find(':last');
                    softFocus.css('background', 'url("' + sImg.attr('src') + '")');
                    softFocus.css('opacity', 0.5);
                    noTrans = true;
                    softFocus.fadeIn(500);
                }

                if (!noTrans) {
                    lens.css('opacity', opts.lensOpacity);										
                }
				if ( opts.position !== 'inside' ) { lens.fadeIn(500); }

                // Start processing. 
                zw.controlLoop();

                return; // Don't return false here otherwise opera will not detect change of the mouse pointer type.
            });
        };

        img1 = new Image();
        $j(img1).load(function () {
            ctx.init2(this, 0);
        });
        img1.src = sImg.attr('src');

        img2 = new Image();
        $j(img2).load(function () {
            ctx.init2(this, 1);
        });
        img2.src = jWin.attr('href');
    }

    $j.fn.CloudZoom = function (options) {
        // IE6 background image flicker fix
        try {
            document.execCommand("BackgroundImageCache", false, true);
        } catch (e) {}
        this.each(function () {
			var	relOpts, opts;
			// Hmm...eval...slap on wrist.
			eval('var	a = {' + $j(this).attr('rel') + '}');
			relOpts = a;
            if ($j(this).is('.cloud-zoom')) {
                $j(this).css({
                    'position': 'relative',
                    'display': 'block'
                });
                $j('img', $j(this)).css({
                    'display': 'block'
                });
                // Wrap an outer div around the link so we can attach things without them becoming part of the link.
                // But not if wrap already exists.
                if ($j(this).parent().attr('id') != 'wrap') {
                    $j(this).wrap('<div id="wrap" style="top:0px;z-index:9999;position:relative;"></div>');
                }
                opts = $j.extend({}, $j.fn.CloudZoom.defaults, options);
                opts = $j.extend({}, opts, relOpts);
                $j(this).data('zoom', new CloudZoom($j(this), opts));

            } else if ($j(this).is('.cloud-zoom-gallery')) {
                opts = $j.extend({}, relOpts, options);
                $j(this).data('relOpts', opts);
                $j(this).bind('click', $j(this), function (event) {
                    var data = event.data.data('relOpts');
                    // Destroy the previous zoom
                    $j('#' + data.useZoom).data('zoom').destroy();
                    // Change the biglink to point to the new big image.
                    $j('#' + data.useZoom).attr('href', event.data.attr('href'));
                    // Change the small image to point to the new small image.
                    $j('#' + data.useZoom + ' img').attr('src', event.data.data('relOpts').smallImage);
                    // Init a new zoom with the new images.				
                    $j('#' + event.data.data('relOpts').useZoom).CloudZoom();
                    return false;
                });
            }
        });
        return this;
    };

    $j.fn.CloudZoom.defaults = {
        zoomWidth: '365',
        zoomHeight: 'auto',
        position: 'right',
        tint: false,
        tintOpacity: 0.5,
        lensOpacity: 0.5,
        softFocus: false,
        smoothMove: 3,
        showTitle: false,
        titleOpacity: 0.5,
        adjustX: -0,
        adjustY: 0
    };

})(jQuery);




$j(document).ready(function() {
	
 
	function megaHoverOver(){
		$j(this).find(".sub").stop().fadeTo('fast', 1).show();
			
		//Calculate width of all ul's
		(function($j) { 
			jQuery.fn.calcSubWidth = function() {
				rowWidth = 0;
				//Calculate row
				$j(this).find("ul").each(function() {					
					rowWidth += $j(this).width(); 
				});	
			};
		})(jQuery); 
		
		if ( $j(this).find(".row").length > 0 ) { //If row exists...
			var biggestRow = 0;	
			//Calculate each row
			$j(this).find(".row").each(function() {							   
				$j(this).calcSubWidth();
				//Find biggest row
				if(rowWidth > biggestRow) {
					biggestRow = rowWidth;
				}
			});
			//Set width
			$j(this).find(".sub").css({'width' :biggestRow});
			$j(this).find(".row:last").css({'margin':'0'});
			
		} else { //If row does not exist...
			
			$j(this).calcSubWidth();
			//Set Width
			$j(this).find(".sub").css({'width' : rowWidth});
			
		}
	}
	
	function megaHoverOut(){ 
	  $j(this).find(".sub").stop().fadeTo('fast', 0, function() {
		  $j(this).hide(); 
	  });
	}
 

 
	
	var config = {    
		 sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)    
		 interval: 0, // number = milliseconds for onMouseOver polling interval    
		 over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
		 timeout: 0, // number = milliseconds delay before onMouseOut    
		 out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
	};
 
	$j("ul#topnav li .sub").css({'opacity':'0'});
	$j("ul#topnav li").hoverIntent(config);
	$j("ul#topnav2 li .sub").css({'opacity':'0'});
	$j("ul#topnav2 li").hoverIntent(config);
 
 
 
});
 
 	function wishl_suc(){ 
	  $j('#ase_mask').css('display', 'block');
	  $j('#ase_dialog1').css('display', 'block');
	}
	
	function wishl_error(){ 
	  $j('#ase_mask').css('display', 'block');
	  $j('#ase_dialog1').css('display', 'block');
	}
 
/////////////////////////////////////////////////////////
 $j(document).ready(function(){
    $j('.check_option').on('click',function(){

        var  t = '<div>'+$j(this).parent().parent().text()+'</div>';

            if($j(this).attr('checked') == 'checked'){
                $j(this).parent().parent().addClass('checked');
                t = $j('#selbrend').html() + t;
                 $j('#selbrend').html(t);
            }
            else{
                $j(this).parent().parent().removeClass('checked');
                var s = $j('#selbrend').html();
                s = s.replace(t , '');
                $j('#selbrend').html(s);
            }
        $j('#whiteblock').css('height', parseInt($j('#colum_preference_select').css('height'))+4+'px');

        //test_check();
    });

    $j('#select_all').on('click',function(){
        var t = '';
        $j('.check_option').each(function(){
            $j(this).attr('checked', 'checked');
            $j(this).parent().parent().addClass('checked');

           t = t + '<div>'+$j(this).parent().parent().text()+'</div>';
        });
          $j('#selbrend').html(t);
          $j('#whiteblock').css('height', parseInt($j('#colum_preference_select').css('height'))+4+'px');

    });

    $j('#deselect_all').on('click',function(){
        $j('.check_option').each(function(){
            $j(this).removeAttr('checked');
            $j(this).parent().parent().removeClass('checked');
        });
         $j('#selbrend').html('');
         $j('#whiteblock').css('height', parseInt($j('#colum_preference_select').css('height'))+4+'px');
    });


     test_check();
 });

///////////////////////////////////////
 $j(document).ready(function(){
    $j('.select_button').on('click', function(){
        //$j('.drop_down_list').css('display', 'none');
        $j(this).find('.drop_down_list').toggle();
        $j(this).toggleClass("highlight");
    });

     $j('.drop_down_item').on('click', function(){
        //$j(this).find('.drop_down_list').css('display', 'block');
        $j(this).parent().parent().find('.value_option',0).text($j(this).text());
    });
 });

function test_check(){
    var t = '';
    $j('.check_option').each(function(){
        if($j(this).attr('checked') == 'checked'){
            $j(this).parent().parent().addClass('checked');
            t = t + '<div>'+$j(this).parent().parent().text()+'</div>';
        }
        else{
            $j(this).parent().parent().removeClass('checked');
        }
    });

    $j('#selbrend').html(t);
    $j('#whiteblock').css('height', parseInt($j('#colum_preference_select').css('height'))+4+'px');
}