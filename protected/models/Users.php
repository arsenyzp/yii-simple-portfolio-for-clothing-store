<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $hash
 * @property integer $active
 * @property string $created
 * @property integer $regip
 * @property integer $user_role_id
 *  @property integer $password2
 */

class Users extends CActiveRecord
{
    const SCENARIO_SIGNUP = 'signup';
    public $password_repeat;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, password', 'required'),
			array('active, regip, user_role_id', 'numerical', 'integerOnly'=>true),
			array('login, password', 'length', 'max'=>255),
			array('hash', 'length', 'max'=>64),
			array('created', 'safe'),
            /////
            // Длина логина должна быть в пределах от 5 до 30 символов
            array('login', 'length', 'min'=>5, 'max'=>100),
            // Логин должен соответствовать шаблону
            //array('login', 'match', 'pattern'=>'/^[A-z][\w]+$/'),
            array('login', 'match', 'pattern'=>'/^(([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9\-]+)\.[a-zA-Z0-9\-.]+$)/'),
            // Логин должен быть уникальным
            array('login', 'unique'),
            // Длина пароля не менее 2 символов
            array('password', 'length', 'min'=>2, 'max'=>30),
            // Повторный пароль и почта обязательны для сценария регистрации
            array('password_repeat', 'required', 'on'=>self::SCENARIO_SIGNUP),
            // Длина повторного пароля не менее 2 символов
            array('password_repeat', 'length', 'min'=>2, 'max'=>30),
            // Пароль должен совпадать с повторным паролем для сценария регистрации
            array('password', 'compare', 'compareAttribute'=>'password_repeat', 'on'=>self::SCENARIO_SIGNUP),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login, password, hash, active, created, regip, user_role_id, first_name, last_name, facebook_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
             'user'=>array(self::BELONGS_TO, 'UserDays', array('id'=>'user_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Email',
			'password' => 'Password',
			'hash' => 'Hash',
			'active' => 'Active',
			'created' => 'Created',
			'regip' => 'Regip',
			'user_role_id' => 'User Role',
            'first_name' => 'First Name',
            'last_name' => 'Last name',
            'facebook_id' => 'Facebook',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('hash',$this->hash,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('regip',$this->regip);
		$criteria->compare('user_role_id',$this->user_role_id);
        $criteria->compare('first_name',$this->first_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    // Метод, который будет вызываться до сохранения данных в БД
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                // Время регистрации
                $this->created = time(1);
                // Хешировать пароль
                $this->password = $this->hashPassword($this->password);

            }

            return true;
        }

        return false;
    }

    protected function afterSave(){
        $user_brands = Yii::app()->session->get('brands');
        foreach($user_brands as $brand=>$v){
            $b = new UserBrands();
            $b->brand_id = $brand;
            $b->user_id = $this->id;
            $b->save();

        }
/*
        $user_categories = Yii::app()->session->get('category');
        foreach($user_categories as $category=>$v){
            $c = new UserCategories();
            $c->category_id = $category;
            $c->user_id = $this->id;
            $c->save();
        }
*/
        /*
        $user_sizes = Yii::app()->session->get('size');
        foreach($user_sizes as $size=>$v){
            $c = new UserSize();
            $c->size_id = $size;
            $c->user_id = $this->id;
            $c->save();
        }
*/
        $days = Yii::app()->session->get('welcom');
        foreach($days as $v){
            if($v == 0)
                continue;
            $d = new UserDays();
            $d->day = $v;
            $d->user_id = $this->id;
            $d->save();
        }
        ////////////Send Email //////////
        $preference = new PreferenceProducts();
        $preference->sendEmailUser($this->id, $this->login, $this->first_name." ".$this->last_name);
    }

    public function validatePassword($password) {
        return crypt($password, $this->password) === $this->password;
    }

    public function hashPassword($password) {
        return crypt($password, $this->blowfishSalt());
    }

    /**
     * Generate a random salt in the crypt(3) standard Blowfish format.
     *
     * @param int $cost Cost parameter from 4 to 31.
     *
     * @throws Exception on invalid cost parameter.
     * @return string A Blowfish hash salt for use in PHP's crypt()
     */
    function blowfishSalt($cost = 13) {
        if (!is_numeric($cost) || $cost < 4 || $cost > 31) {
            throw new Exception("cost parameter must be between 4 and 31");
        }
        // Get some pseudo-random data from mt_rand().
        $rand = '';
        for ($i = 0; $i < 8; ++$i)
            $rand.=pack('S', mt_rand(0, 0xffff));
        // Add the microtime for a little more entropy.
        $rand.=microtime();
        // Mix the bits cryptographically.
        $rand = sha1($rand, true);
        // Form the prefix that specifies hash algorithm type and cost parameter.
        $salt = '$2a$' . str_pad((int) $cost, 2, '0', STR_PAD_RIGHT) . '$';
        // Append the random salt string in the required base64 format.
        $salt.=strtr(substr(base64_encode($rand), 0, 22), array('+' => '.'));
        return $salt;
    }

    public function passwordGenerator(){
        return rand(100, 10000);
    }

    public function setPasswd(){
        $this->password = $this->passwordGenerator();
        $this->password_repeat = $this->password;
    }

}