<?php

/**
 * This is the model class for table "properties".
 *
 * The followings are the available columns in table 'properties':
 * @property integer $id
 * @property integer $id_catigories
 * @property string $name
 * @property string $description
 */
class Properties extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Properties the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'properties';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_catigories, name', 'required'),
			array('id_catigories', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>512),
			array('description', 'length', 'max'=>1024),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_catigories, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_catigories' => 'Id Catigories',
			'name' => 'Name',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_catigories',$this->id_catigories);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getGroupName(){
        $name = PropertyCategory::model()->findByPk($this->id_catigories);
       return $name->name;
    }

    public function getPropertiesOrderGroup(){
        $result = array();
        $groups = PropertyCategory::model()->findAll(array('order' => 'name ASC'));
        foreach($groups as $group){
            $properties = $this->findAllByAttributes(array('id_catigories'=>$group->id));
            foreach($properties as $property){
                $result[$group->name][] = array('name'=>$property->name, 'id'=>$property->id);
            }
        }
        return $result;
    }
}