<?php

/**
 * This is the model class for table "wp_fp_brand_mapping".
 *
 * The followings are the available columns in table 'wp_fp_brand_mapping':
 * @property integer $brand_map_id
 * @property string $brand_name
 * @property string $wp_brand_name
 */
class WpFpBrandMapping extends CActiveRecord
{
    public $popular;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WpFpBrandMapping the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wp_fp_brand_mapping';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_name, wp_brand_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('brand_map_id, brand_name, wp_brand_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'p'=>array(self::HAS_ONE, 'PopularBrands', 'brand_map_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'brand_map_id' => 'Brand Map',
			'brand_name' => 'Brand Name',
			'wp_brand_name' => 'Wp Brand Name',
            'popular' => 'Popular'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('brand_map_id',$this->brand_map_id);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('wp_brand_name',$this->wp_brand_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function popular(){
        // Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria(array(
            'condition'=>'p.brand_map_id > 0',
            'with' => array('p'),
        ));

		//$criteria->compare('brand_map_id',$this->brand_map_id);
		//$criteria->compare('brand_name',$this->brand_name,true);
		//$criteria->compare('wp_brand_name',$this->wp_brand_name,true);
        //$criteria->addCondition('brand_map_id > 0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }

    public function isPopular(){
       return (bool) PopularBrands::model()->findByAttributes(array('brand_map_id'=>$this->brand_map_id));
    }
}