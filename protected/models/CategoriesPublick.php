<?php

/**
 * This is the model class for table "categories_publick".
 *
 * The followings are the available columns in table 'categories_publick':
 * @property integer $categories_id
 */
class CategoriesPublick extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoriesPublick the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categories_publick';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categories_id', 'required'),
			array('categories_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('categories_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'categories_id' => 'Categories',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('categories_id',$this->categories_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function updateList($list){
        foreach($list as $k=>$v){
            if($v>0){
                $item = $this->findByAttributes(array('categories_id'=>$k));
                if(!$item){
                    $item = new CategoriesPublick();
                    $item->categories_id = $k;
                    $item->save();
                }
            }
            else{
                $this->deleteByPk($k);
            }
        }
    }

    public function getListCategoriesPublick(){
    	$model = new WpTerms();
        $q = new CDbCriteria(array(
                'condition'=>'p.categories_id > 0',
                'with' => array('p'),
            ));
        return $model->findAll($q);
    }
}