<?php

/**
 * This is the model class for table "link_size".
 *
 * The followings are the available columns in table 'link_size':
 * @property integer $id
 * @property integer $size_id
 * @property integer $properties_id
 */
class LinkSize extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LinkSize the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'link_size';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('size_id, properties_id', 'required'),
			array('size_id, properties_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, size_id, properties_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'p'=>array(self::HAS_ONE, 'ProductsProperties', 'properties_id'),
            's'=>array(self::BELONGS_TO, 'Size', 'size_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'size_id' => 'Size',
			'properties_id' => 'Properties',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('size_id',$this->size_id);
		$criteria->compare('properties_id',$this->properties_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function updateList($list){
        foreach($list as $k=>$v){
            if($v>0){
                $item = $this->findByAttributes(array('properties_id'=>$k, 'size_id'=>$v));
                if(!$item){
                    $item = new LinkSize();
                    $item->properties_id = $k;
                    $item->size_id = $v;
                    $item->save();
                }
            }
            else{
                $this->deleteByPk($k);
            }
        }
    }
}