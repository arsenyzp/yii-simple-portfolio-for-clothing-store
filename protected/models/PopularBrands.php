<?php

/**
 * This is the model class for table "popular_brands".
 *
 * The followings are the available columns in table 'popular_brands':
 * @property integer $id
 * @property integer $brand_map_id
 */
class PopularBrands extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PopularBrands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'popular_brands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_map_id', 'required'),
			array('brand_map_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('brand_map_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'brands'=>array(self::HAS_ONE, 'WpFpBrandMapping', 'brand_map_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'brand_map_id' => 'Brand Map',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('brand_map_id',$this->brand_map_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function updateList($list){
        foreach($list as $k=>$v){
            if($v>0){
                $item = $this->findByAttributes(array('brand_map_id'=>$k));
                if(!$item){
                    $item = new PopularBrands();
                    $item->brand_map_id = $k;
                    $item->save();
                }
            }
            else{
                    $this->deleteByPk($k);
            }
        }
    }
}