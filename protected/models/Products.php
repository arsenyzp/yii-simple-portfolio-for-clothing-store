<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property string $official_id
 * @property string $product_name
 * @property string $brand_name
 * @property string $merchant_name
 * @property string $merchant_cat
 * @property string $network_cat
 * @property string $price
 * @property string $was_price
 * @property string $image
 * @property string $image_large
 * @property string $image_thumb
 * @property string $more_images
 * @property string $description
 * @property string $description2
 * @property string $product_url
 * @property string $colour
 * @property string $sizes
 * @property integer $stock
 * @property string $stock_text
 * @property string $delivery_cost
 * @property string $status
 * @property string $active
 * @property integer $rule_id
 * @property string $export_cat
 * @property string $export_cat_name
 * @property string $image_checked
 * @property integer $image_display_width
 * @property integer $image_display_height
 * @property integer $match_id
 * @property integer $date_categorised
 * @property integer $date_first_imported
 * @property integer $date_last_imported
 * @property integer $datafeed_id
 * @property string $gender
 * @property integer $image_copied
 * @property string $kids
 */
class Products extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, kids', 'required'),
			array('stock, rule_id, image_display_width, image_display_height, match_id, date_categorised, date_first_imported, date_last_imported, datafeed_id, image_copied', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>50),
			array('official_id, product_name, brand_name, merchant_name, merchant_cat, network_cat, colour, stock_text, export_cat, export_cat_name, gender', 'length', 'max'=>255),
			array('price, was_price, delivery_cost', 'length', 'max'=>10),
			array('image, image_large, image_thumb, more_images, description, description2, product_url, sizes', 'length', 'max'=>1024),
			array('status', 'length', 'max'=>5),
			array('active', 'length', 'max'=>1),
			array('image_checked, kids', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, official_id, product_name, brand_name, merchant_name, merchant_cat, network_cat, price, was_price, image, image_large, image_thumb, more_images, description, description2, product_url, colour, sizes, stock, stock_text, delivery_cost, status, active, rule_id, export_cat, export_cat_name, image_checked, image_display_width, image_display_height, match_id, date_categorised, date_first_imported, date_last_imported, datafeed_id, gender, image_copied, kids', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'official_id' => 'Official',
			'product_name' => 'Product Name',
			'brand_name' => 'Brand Name',
			'merchant_name' => 'Merchant Name',
			'merchant_cat' => 'Merchant Cat',
			'network_cat' => 'Network Cat',
			'price' => 'Price',
			'was_price' => 'Was Price',
			'image' => 'Image',
			'image_large' => 'Image Large',
			'image_thumb' => 'Image Thumb',
			'more_images' => 'More Images',
			'description' => 'Description',
			'description2' => 'Description2',
			'product_url' => 'Product Url',
			'colour' => 'Colour',
			'sizes' => 'Sizes',
			'stock' => 'Stock',
			'stock_text' => 'Stock Text',
			'delivery_cost' => 'Delivery Cost',
			'status' => 'Status',
			'active' => 'Active',
			'rule_id' => 'Rule',
			'export_cat' => 'Export Cat',
			'export_cat_name' => 'Export Cat Name',
			'image_checked' => 'Image Checked',
			'image_display_width' => 'Image Display Width',
			'image_display_height' => 'Image Display Height',
			'match_id' => 'Match',
			'date_categorised' => 'Date Categorised',
			'date_first_imported' => 'Date First Imported',
			'date_last_imported' => 'Date Last Imported',
			'datafeed_id' => 'Datafeed',
			'gender' => 'Gender',
			'image_copied' => 'Image Copied',
			'kids' => 'Kids',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('official_id',$this->official_id,true);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('merchant_name',$this->merchant_name,true);
		$criteria->compare('merchant_cat',$this->merchant_cat,true);
		$criteria->compare('network_cat',$this->network_cat,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('was_price',$this->was_price,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('image_large',$this->image_large,true);
		$criteria->compare('image_thumb',$this->image_thumb,true);
		$criteria->compare('more_images',$this->more_images,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('description2',$this->description2,true);
		$criteria->compare('product_url',$this->product_url,true);
		$criteria->compare('colour',$this->colour,true);
		$criteria->compare('sizes',$this->sizes,true);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('stock_text',$this->stock_text,true);
		$criteria->compare('delivery_cost',$this->delivery_cost,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('active',$this->active,true);
		$criteria->compare('rule_id',$this->rule_id);
		$criteria->compare('export_cat',$this->export_cat,true);
		$criteria->compare('export_cat_name',$this->export_cat_name,true);
		$criteria->compare('image_checked',$this->image_checked,true);
		$criteria->compare('image_display_width',$this->image_display_width);
		$criteria->compare('image_display_height',$this->image_display_height);
		$criteria->compare('match_id',$this->match_id);
		$criteria->compare('date_categorised',$this->date_categorised);
		$criteria->compare('date_first_imported',$this->date_first_imported);
		$criteria->compare('date_last_imported',$this->date_last_imported);
		$criteria->compare('datafeed_id',$this->datafeed_id);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('image_copied',$this->image_copied);
		$criteria->compare('kids',$this->kids,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}