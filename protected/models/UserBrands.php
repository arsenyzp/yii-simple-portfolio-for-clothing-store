<?php

/**
 * This is the model class for table "user_brands".
 *
 * The followings are the available columns in table 'user_brands':
 * @property integer $id
 * @property integer $user_id
 * @property integer $brand_id
 */
class UserBrands extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserBrands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_brands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, brand_id', 'required'),
			array('user_id, brand_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, brand_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			 'p'=>array(self::HAS_ONE, 'Brands', array('id'=>'brand_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'brand_id' => 'Brand',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('brand_id',$this->brand_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function updateUserBrands($user_id, $array_brands){
		if($user_id == 0)
			return;
       $delete = array();
            foreach ($array_brands as $k => $v) {
                if($v == 0){
                    $delete[] = $k;
                    continue;
                }
                else{
                    $b = new UserBrands();
                    if($b->countByAttributes(array('brand_id'=>$v, 'user_id'=>$user_id)) == 0){
                        $b->brand_id = $v;
                        $b->user_id = $user_id;
                        $b->save();
                    }
                }
            }
            if(count($delete)>0){
                $connection=Yii::app()->db;
                $q = "DELETE FROM `user_brands` WHERE `user_id` = ".$user_id." AND `brand_id` IN (".implode(',', $delete).")";
                $connection->createCommand($q)->query();
            }
	}
}