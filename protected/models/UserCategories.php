<?php

/**
 * This is the model class for table "user_categories".
 *
 * The followings are the available columns in table 'user_categories':
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 */
class UserCategories extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, category_id', 'required'),
			array('user_id, category_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'p'=>array(self::HAS_ONE, 'WpTerms', array('term_id'=>'category_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'category_id' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function updateUserCategories($user_id, $array_categories){
		if($user_id == 0)
			return;
		$this->deleteAllByAttributes(array('user_id'=>$user_id));
		foreach ($array_categories as $k => $v) {
			$c = new UserCategories;
			$c->category_id = $k;
			$c->user_id = $user_id;
			$c->save();
		}
	}

	public function updateUserCategory($user_id, $category_id = 0, $status = 0){
		if($user_id == 0)
			return;
		if($status == 0){
			return  $this->deleteAllByAttributes(array('user_id'=>$user_id, 'category_id'=>$category_id));
		}
		$c = new UserCategories;
		$c->category_id = $category_id;
		$c->user_id = $user_id;
		return $c->save();
	}

	public function updateUserCategorytemp($user_id, $category_id = 0, $status = 0){
		if($user_id == 0)
			return;
		$array_tmp_category =  Yii::app()->session->get("tcat_preference");
		if($status == 0){
			if(count($array_tmp_category) == 0){
				$arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();
            $selected_category = array();
                foreach($arrayCategory as $c){
                    $selected_category[$c->term_id] = $c->term_id;
                }
                $array_tmp_category = $selected_category;
			}
			 $array_tmp_category = array_diff($array_tmp_category, array($category_id));
			return Yii::app()->session->add("tcat_preference", $array_tmp_category);
		}
		$array_tmp_category = array_merge($array_tmp_category, array($category_id));
		return Yii::app()->session->add("tcat_preference", $array_tmp_category);
	}

	public function setAllCategories($user_id){
		$arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();
        $selected_category = array();
            foreach($arrayCategory as $c){
                $selected_category[$c->term_id] = $c->term_id;
            }
        $this->updateUserCategories($user_id, $selected_category);
	}

	public function setNoneCategories($user_id){
        $this->updateUserCategories($user_id, array());
	}
}