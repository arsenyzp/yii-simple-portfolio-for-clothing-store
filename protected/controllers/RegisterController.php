<?php

class RegisterController extends Controller
{
    public $layout='//layouts/milanstyle';

	public function actionIndex()
	{
        if(Yii::app()->session->get("id_us"))
            $this->redirect(array('cabinet/index/user/'.Yii::app()->session->get("id_us")));
        else
            $this->redirect(array('register/step1'));
	}

    public function actionStep1()
	{
        if(Yii::app()->session->get("id_us"))
            $this->redirect(array('cabinet/index/user/'.Yii::app()->session->get("id_us")));
        if(isset($_POST['brands']))
        {
            $brends = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'propert_') !== false){
                    $brends[intval(str_replace('propert_', '', $name))] = $data;
                }
            }
            Yii::app()->session->add("brands", $brends);
             //$this->redirect(array('register/step2'));
            /***********************/



            /***********************/
            $this->redirect(array('register/step3'));
        }
        else{
            $model = new Brands();
            if(!isset($_GET['all'])){
                $q = new CDbCriteria(array(
                    'condition'=>'p.brand_map_id > 0',
                    'with' => array('p'),
                    'order' => 'brands',
                ));
                $arrayProperties = $model->findAll($q);
            }
            else{
                $arrayProperties = $model->findAll(array('order'=>'brands'));
            }
            $brands_select = array();
            if(count(Yii::app()->session->get("brands")) > 0){
                $brands_select = Yii::app()->session->get("brands");
            }

            $this->render('step1', array('list'=>$arrayProperties, 'selected'=>$brands_select));
        }
	}

    public function actionStep2(){

        if(count(Yii::app()->session->get("brands")) == 0)
            $this->redirect(array('register/step1'));
        if(Yii::app()->session->get("id_us"))
            $this->redirect(array('cabinet/index/user/'.Yii::app()->session->get("id_us")));

        if(isset($_POST['step']))
        {
            $category = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'category_') !== false){
                    $category[intval(str_replace('category_', '', $name))] = $data;
                }
            }

            ///////////////////////////

            $size_array = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'size_') !== false){
                    $size_array[intval(str_replace('size_', '', $name))] = $data;
                }
            }

            ///////////////////////////
            Yii::app()->session->add("size", $size_array);
            Yii::app()->session->add("category", $category);
            $this->redirect(array('register/step3'));
        }
        else{
            $arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();

            $selected_category = array();
            if(count(Yii::app()->session->get("category")) > 0){
                $selected_category = Yii::app()->session->get("category");
            }
            else{
                foreach($arrayCategory as $c){
                    $selected_category[$c->term_id] = $c->term_id;
                }
            }

            ///////////////////////////////

            $listSize = Size::model()->getListSize();

            $selected_size = array();
            if(count(Yii::app()->session->get("size")) > 0){
                $selected_size = Yii::app()->session->get("size");
            }
            ///////////////////////////////


            $this->render('step2', array('list'=>$arrayCategory, 'selected'=>$selected_category, 'listSize'=>$listSize, 'selected_size'=>$selected_size));
        }
    }

    public function actionStep3(){

        if(! Yii::app()->session->get("brands"))
            $this->redirect(array('register/step1'));
       // if(! Yii::app()->session->get("category"))
        //    $this->redirect(array('register/step2'));
        if(Yii::app()->session->get("regid"))
            $this->redirect(array('register/welcom'));
        if(Yii::app()->session->get("id_us"))
            $this->redirect(array('cabinet/index/user/'.Yii::app()->session->get("id_us")));

        $model = new RegisterForm();


        if(isset($_POST['RegisterForm']))
        {
            $model->attributes=$_POST['RegisterForm'];
            $day = array();
            foreach($model->day as $v){
                    if($v == 0)
                        continue;
                $day[$v]=$v;
            }
            Yii::app()->session->add("welcom",$day);

            if($_POST['facebook']!='facebook')
            if($model->validate()){
                   ///////////////////
                $user = new Users();
                $user->login = $model->email;
                $user->setPasswd();
                $user->first_name = $model->firstname;
                $user->last_name = $model->lastname;
                $user->facebook_id = '';
                $user->save();
                Yii::app()->session->add("regid", $user->first_name);
                 Yii::app()->session->add("id_us", $user->id);
                $this->redirect(array('register/welcom'));

            }
            /*
            $selected_day = array();
            if(count(Yii::app()->session->get("welcom")) > 0){
                $selected_day = Yii::app()->session->get("welcom");
            }
            $this->render('step3', array('model'=>$model, 'selected'=>$selected_day));
            */
        }
            $selected_day = array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7);
            if(count(Yii::app()->session->get("welcom")) > 0){
                $selected_day = Yii::app()->session->get("welcom");
            }
            $this->render('step3', array('model'=>$model, 'selected'=>$selected_day));
    }

    public function actionWelcom(){
        if(!Yii::app()->session->get("welcom"))
            $this->redirect(array('register/step3'));
        $username = Yii::app()->session->get("regid") ? Yii::app()->session->get("regid") : 'TOM';
        $id_us = Yii::app()->session->get("id_us") ? Yii::app()->session->get("id_us") : '0';
        $this->render('welcom', array('username'=>$username, 'id_us'=>$id_us));
    }

    /**
     * Register User
     */
/*
    public function actionRegister()
    {
        $model=new Users(Users::SCENARIO_SIGNUP);

        // uncomment the following code to enable ajax-based validation

        if(isset($_POST['ajax']) && $_POST['ajax']==='users-register-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            if($model->validate())
            {
                // form inputs are valid, do something here
                $model->active = 1;
                $model->created = time();
                $model->regip = 0;
                $model->user_role_id = 0;
                $model->password = $model->hashPassword($model->password);
                if ($model->save(false))
                    $preference = Yii::app()->session->get("preference");
                    foreach($preference as $k=>$v){
                        $p = new Propertiesforus();
                        $p->id_property = $k;
                        $p->id_user = $model->id;
                        $p->save(false);
                    }
                    $us = new UserIdentity($model->login, $model->password);
                    $us->authenticate();
                    Yii::app()->user->login($us,0);
                    $us->setState('id', $model->id);
                    $this->redirect(array('/cabinet'));
                return;
            }
        }
        $this->render('register',array('model'=>$model));
    }
*/
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}