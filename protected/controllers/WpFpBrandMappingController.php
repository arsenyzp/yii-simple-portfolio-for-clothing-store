<?php

class WpFpBrandMappingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index'),
				'users'=>array('admin'),
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('popular'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
        if(isset($_POST['popular'])){
            $popular_list = array();
            foreach($_POST as $k=>$item){
                if(intval($k) > 0)
                    $popular_list[intval($k)] = $item;
            }
            PopularBrands::model()->updateList($popular_list);
        }
		$model=new WpFpBrandMapping('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WpFpBrandMapping']))
			$model->attributes=$_GET['WpFpBrandMapping'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    	/**
	 * Manages all models.
	 */
	public function actionPopular()
	{
        if(isset($_POST['popular'])){
            $popular_list = array();
            foreach($_POST as $k=>$item){
                if(intval($k) > 0)
                    $popular_list[intval($k)] = $item;
            }
            PopularBrands::model()->updateList($popular_list);
        }
		$model=new WpFpBrandMapping('search');
		$model->unsetAttributes();  // clear any default values
        //$model->attributes = array('popular.id'=>'1');
		if(isset($_GET['WpFpBrandMapping']))
			$model->attributes=$_GET['WpFpBrandMapping'];
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=WpFpBrandMapping::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wp-fp-brand-mapping-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
