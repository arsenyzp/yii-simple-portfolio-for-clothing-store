<?php

class CabinetController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/cabinet';

	public function actionIndex($user = 0)
	{
        if($user > 0)
            Yii::app()->session->add("id_us", $user);
        $user_profile = Users::model()->findByAttributes(array('id'=>intval($user)));
        //$user_day = UserDays::model()->finndByAttributes();$model = new WpTerms();
        $model = new WpTerms();
        $q = new CDbCriteria(array(
            'condition'=>'p.categories_id > 0',
            'with' => array('p'),
        ));
        $categories = $model->findAll($q);

        $crit = new CDbCriteria;
        $crit->select = "category_id";
        $crit->condition  = "user_id = :user_id";
        $crit->params = array(':user_id' =>$user);
        //получаем выбранные категории из базы данных
/*
        $user_categories = UserCategories::model()->findAll($crit);

        $array_t = array();
        foreach ($user_categories as $v) {
            $array_t[] = $v->category_id;
        }
*/
        //получаем выбранные категории из сессии
        $user_categories = Yii::app()->session->get("tcat_preference");
        //если категорий нет то выбираем все опубликованные категории
        if(count($user_categories) == 0){
            $arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();
            $selected_category = array();
                foreach($arrayCategory as $c){
                    $selected_category[$c->term_id] = $c->term_id;
                }
            //Yii::app()->session->add("tcat_preference", $selected_category);
            $user_categories = $selected_category;
        }

        $user_brands = UserBrands::model()->with('p')->findAllByAttributes(array('user_id'=>$user),array('order'=>'p.brands ASC'));
        $listSize = Size::model()->getListSize();
        $user_size = UserSize::model()->findAllByAttributes(array('user_id'=>$user));
        $selected_size = array();
        foreach ($user_size as $v) {
            $selected_size[] = $v->size_id;
        }
        $sale_preference = Yii::app()->session->get("sale_preference");
        $pp = new PreferenceProducts;
        $template_vars = $pp->getPreferenceProductUser($user);
        $this->render('index', array(
                    'user_name'=>$user_profile->first_name." ".$user_profile->last_name,
                    'user_id'=>$user, 
                    'user_brands'=>$user_brands, 
                    'user_categories'=>$user_categories,
                    'listSize'=>$listSize,
                    'selected_size'=>$selected_size,
                    'categories'=>$categories,
                    'list_product'=>$template_vars,
                    'sale_preference'=>$sale_preference,
                     ));
	}

    public function actionSizes($user = 0){

         if(isset($_POST['step']))
        {
            $size_array = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'size_') !== false){
                    $size_array[intval(str_replace('size_', '', $name))] = $data;
                }
            }
            UserSize::model()->updateUserSize($user, $size_array);
            $this->redirect(array('cabinet/index/user/'.$user));
        }

        $listSize = Size::model()->getListSize();

        $user_size = UserSize::model()->findAllByAttributes(array('user_id'=>$user));
        $selected_size = array();
        foreach ($user_size as $v) {
            $selected_size[] = $v->size_id;
        }

        $this->render('sizes', array('listSize'=>$listSize, 'selected_size'=>$selected_size));
    }

    public function actionCategories($user = 0){

         if(isset($_POST['step']))
        {
            $category = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'category_') !== false){
                    $category[intval(str_replace('category_', '', $name))] = $data;
                }
            }
            UserCategories::model()->updateUserCategories($user, $category);
            $this->redirect(array('cabinet/index/user/'.$user));
        }

        $arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();

        $crit = new CDbCriteria;
        $crit->select = "category_id";
        $crit->condition  = "user_id = :user_id";
        $crit->params = array(':user_id' =>$user);
        $user_categories = UserCategories::model()->findAll($crit);

        $selected_category = array();
        foreach ($user_categories as $v) {
            $selected_category[] = $v->category_id;
        }

        $this->render('categories', array('list'=>$arrayCategory, 'selected'=>$selected_category));
    }

    public function actionBrands($user = 0){
        if(isset($_POST['brands'])){
            $brends = array();
            foreach($_POST as $name=>$data){
                if(strpos($name, 'propert_') !== false){
                    $brends[intval(str_replace('propert_', '', $name))] = $data;
                }
            }

            //print_r($brends);
            UserBrands::model()->updateUserBrands($user, $brends);
            $this->redirect(array('cabinet/index/user/'.$user));
        }
        $model = new Brands();
            if(!isset($_GET['all'])){
                $q = new CDbCriteria(array(
                    'condition'=>'p.brand_map_id > 0',
                    'with' => array('p'),
                    'order' => 'brands',
                ));
                $arrayProperties = $model->findAll($q);
            }
            else{
                $arrayProperties = $model->findAll(array('order'=>'brands'));
            }

        $user_brands = UserBrands::model()->findAllByAttributes(array('user_id'=>$user));
        $brands_select = array();
         foreach ($user_brands as $v) {
            $brands_select[] = $v->brand_id;
        }
        $this->render('brands', array('list'=>$arrayProperties, 'selected'=>$brands_select, 'user_id'=>$user));
    }

public function actionAjax($user, $page = 0){
    $user = intval($user);
    $page = intval($page);
    if(isset($_GET['id_cat']) && isset($_GET['status'])){
        //update user catigories
        UserCategories::model()->updateUserCategorytemp($user, intval($_GET['id_cat']), intval($_GET['status']));
    }
    if(isset($_GET['all'])){
        //update user catigories SELECT ALL
        if($_GET['all'] == 'all'){
            $arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();
            $selected_category = array();
                foreach($arrayCategory as $c){
                    $selected_category[$c->term_id] = $c->term_id;
                }
            Yii::app()->session->add("tcat_preference", $selected_category);
            }  
        if($_GET['all'] == 'none')
             Yii::app()->session->add("tcat_preference", array());
    }
    if(isset($_GET['sale']) && isset($_GET['status']))
    {
        //save sale preference
        $sale_preference = Yii::app()->session->get("sale_preference");
        if(!is_array($sale_preference)){
            $sale_preference = array();
        }
        $sale_preference[intval($_GET['sale'])] = intval($_GET['status']);
        Yii::app()->session->add("sale_preference",$sale_preference);
    }
    $pp = new PreferenceProducts;
    $template_vars = $pp->getPreferenceProductUser($user, $page);
    $this->renderPartial('products', array(
                    'list_product'=>$template_vars,
                    ));
}

public function actionDays($user = 0){
    $model = new RegisterForm();
    if(isset($_POST['RegisterForm'])){
        $model->attributes=$_POST['RegisterForm'];
        $day = array();
        foreach($model->day as $v){
                if($v == 0)
                    continue;
            $day[$v]=$v;
        }
        Yii::app()->session->add("welcom",$day);
        $this->redirect(array('cabinet/index/user/'.$user));
    }
    $selected_day = array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7);
    if(count(Yii::app()->session->get("welcom")) > 0){
        $selected_day = Yii::app()->session->get("welcom");
    }

    $this->render('days', array('model'=>$model, 'selected'=>$selected_day, 'user_id'=>$user));
}

public function actionDelete($user=0){

}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}