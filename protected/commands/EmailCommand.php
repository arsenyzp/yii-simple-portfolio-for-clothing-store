<?php
class EmailCommand extends CConsoleCommand
{

    public function actionIndex(){
        $preference = new PreferenceProducts();
        $day = date("w", time(1));
        $users = UserDays::model()->with('p')->findAllByAttributes(array('day'=>$day));
        //var_dump($users);
        echo count($users)."\n\r";
        if(count($users)>0)
        foreach($users as $user){
            if(!$user OR !$user->p OR strlen($user->p->login)<4)
                continue;
            echo $user->user_id." ".$user->p->login." ".$user->p->first_name." ".$user->p->last_name."\n\r";
            $preference->sendEmailUser($user->user_id, $user->p->login, $user->p->first_name." ".$user->p->last_name);
        }

        $to = "info@milanstyle.co.uk";
        $subject = "Milan Style | Sending email";
        $body = "Sent  ".count($users);
        $headers = "From: info@milanstyle.co.uk\r\n" .
             "X-Mailer: php";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        if (mail($to, $subject, $body, $headers)) {
           echo("<p>Message successfully sent!</p>");
        } else {
           echo("<p>Message delivery failed...</p>");
        }
    }

    public function actionWelcam($userid){
        $preference = new PreferenceProducts();
        $user = Users::model()->findByAttributes(array('id'=>$userid));
        if($user)
            $preference->sendEmailUser($user->id, $user->login, $user->first_name." ".$user->last_name);
    }

}