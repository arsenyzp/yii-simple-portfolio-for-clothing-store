<?php
$this->breadcrumbs=array(
	'Products Properties'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ProductsProperties','url'=>array('index')),
	array('label'=>'Create ProductsProperties','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('products-properties-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Products Properties</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<?php
/*
  *
  $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'products-properties-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'description',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
));
*/
?>
<form method="POST">
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'size-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'type' => 'raw',
            'value' => '$data->id',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => '$data->name',
        ),
        array(
            'name' => 'size',
            'type' => 'raw',
            'value' => '$data->getRelationsSize()',
            //'filter' => CategoriSizes::model()->getListCategory()
        ),
        /*
        array(
            'name' => 'description',
            'type' => 'raw',
            'value' => '$data->description'
        ),
        */
        /*
          'short_description',
          'description',
          'status',
          'thumbnail',
          'lang',
         */
        /*
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
        */
    ),
));
?>
    <input type="hidden" value="Popular" name="data" />
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Save')); ?>
</form>