<?php
$this->breadcrumbs=array(
	'Products Properties'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ProductsProperties','url'=>array('index')),
	array('label'=>'Create ProductsProperties','url'=>array('create')),
	array('label'=>'Update ProductsProperties','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ProductsProperties','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductsProperties','url'=>array('admin')),
);
?>

<h1>View ProductsProperties #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
