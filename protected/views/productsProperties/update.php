<?php
$this->breadcrumbs=array(
	'Products Properties'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductsProperties','url'=>array('index')),
	array('label'=>'Create ProductsProperties','url'=>array('create')),
	array('label'=>'View ProductsProperties','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ProductsProperties','url'=>array('admin')),
);
?>

<h1>Update ProductsProperties <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>