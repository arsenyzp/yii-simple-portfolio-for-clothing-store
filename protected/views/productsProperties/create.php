<?php
$this->breadcrumbs=array(
	'Products Properties'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductsProperties','url'=>array('index')),
	array('label'=>'Manage ProductsProperties','url'=>array('admin')),
);
?>

<h1>Create ProductsProperties</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>