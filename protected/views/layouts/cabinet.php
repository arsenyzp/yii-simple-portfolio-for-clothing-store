<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html dir="ltr" lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo Yii::app()->request->baseUrl; ?>/css/milanstyle/style.css" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/milanstyle/searchfield.css" rel="stylesheet" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo Yii::app()->request->baseUrl; ?>/css/milanstyle/cabinet.css" />

    <script type="text/javascript" src="http://www.milanstyle.co.uk/wp-includes/js/jquery/jquery.js?ver=1.7.1"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/searchfield.js"></script>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="iehacks.css" />
    <![endif]-->
    <?php /*
    <script language="JavaScript" type="text/javascript">
        (function($) {
            // your code with dollars here
            $(document).ready(function() {

                var container = $('#reorder');
                var children = $('#ord');

                for (var i = 0; i < order.length; i++){
                    container.append(children[order[i]-1])
                }
            });
            // your code with dollars here
        })( jQuery );
    </script>
    */
    ?>
</head>
<body>
<div id="wrapper">

<div id="header">
<div id="logo">
    <a href="/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" title="MilanStyle.co.uk - Men's Designer Clothes from Milan, Paris and London to your Front Door." alt="MilanStyle.co.uk - Men's Designer Clothes from Milan, Paris and London to your Front Door." /></a>
</div>
<div id="header-right">
    <div id="account">


        <ul class="toplink">
            <li>
                <a href="http://milanstyle.co.uk/language/russian/">Россия</a>
                <a href="http://milanstyle.co.uk/language/japanese/">日本語</a>
                <a href="http://milanstyle.co.uk/language/simplified-chinese/">简体中文</a>
                <a href="http://milanstyle.co.uk/language/portugese/">Português</a>
                <a href="http://milanstyle.co.uk/language/korean/">한국어</a>
                <a href="http://milanstyle.co.uk/language/arabic/">العربية</a>


            </li>
        </ul>

    </div>
    <div id="search-bar">
        <form method="GET" action="/search/">
            <div id ="quick_search_wrapper">
                <div id ="quick_search_left">
                    <input type="text"   id="searchfield" name="q" style="width: 187px; background-color:#FFFFFF; height: 12px; border: none; padding:2px; margin:0px; font-size:12px; line-height:12px" onClick="this.value = '';"  />
                </div>
                <div class="sf_suggestion"><ul style="display: none; width: 261px;"></ul></div>
                <div id="quick_search_button">

                    <input type="image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/searchbutton.gif" alt="Search" id="searcbutton">
                </div>
            </div>
        </form>


    </div>
    <div class="cls"></div>
</div>

<div class="skip-link screen-reader-text"><a href="#content" title="test">test</a></div>

<ul id="topnav">
    <li>
        <a href="/designer-directory-a-z/" class="designer">SHOP BY DESIGNER</a>     <div class="sub" id="lat_sub_menu_brand" style="opacity: 0; display: none; width: 795px;">
            <ul>
                <li><a href="http://www.milanstyle.co.uk/alexander-mcqueen">Alexander McQueen</a></li>
                <li><a href="http://www.milanstyle.co.uk/ann-demeulemeester">Ann Demeulemeester</a></li>
                <li><a href="http://www.milanstyle.co.uk/armani">Armani</a></li>
                <li><a href="http://www.milanstyle.co.uk/balenciaga">Balenciaga</a></li>
                <li><a href="http://www.milanstyle.co.uk/balmain">Balmain</a></li>
                <li><a href="http://www.milanstyle.co.uk/belstaff">Belstaff</a></li>
                <li><a href="http://www.milanstyle.co.uk/bottega-veneta">BOTTEGA VENETA</a></li>
                <li><a href="http://www.milanstyle.co.uk/brioni">BRIONI</a></li>
                <li><a href="http://www.milanstyle.co.uk/burberry">BURBERRY</a></li>
                <li><a href="http://www.milanstyle.co.uk/canali">CANALI</a></li>
                <li><a href="http://www.milanstyle.co.uk/comme-des-garcons">Comme des Garcons</a></li>
            </ul>
            <ul>
                <li><a href="http://www.milanstyle.co.uk/damir-doma">DAMIR DOMA</a></li>
                <li><a href="http://www.milanstyle.co.uk/dior-homme">Dior Homme</a></li>
                <li><a href="http://www.milanstyle.co.uk/dolce-gabbana">Dolce &amp; Gabbana</a></li>
                <li><a href="http://www.milanstyle.co.uk/dries-van-noten">Dries Van Noten</a></li>
                <li><a href="http://www.milanstyle.co.uk/dsquared">DSQUARED</a></li>
                <li><a href="http://www.milanstyle.co.uk/ermenegildo-zegna">Ermenegildo Zegna</a></li>
                <li><a href="http://www.milanstyle.co.uk/etro">ETRO</a></li>
                <li><a href="http://www.milanstyle.co.uk/fendi">FENDI</a></li>
                <li><a href="http://www.milanstyle.co.uk/giuseppe-zanotti-design">Giuseppe Zanotti Design</a></li>
                <li><a href="http://www.milanstyle.co.uk/givenchy">GIVENCHY</a></li>
                <li><a href="http://www.milanstyle.co.uk/gucci">Gucci</a></li>
            </ul>
            <ul>
                <li><a href="http://www.milanstyle.co.uk/incotex">INCOTEX</a></li>
                <li><a href="http://www.milanstyle.co.uk/jil-sander">Jil Sander</a></li>
                <li><a href="http://www.milanstyle.co.uk/jimmy-choo">JIMMY CHOO</a></li>
                <li><a href="http://www.milanstyle.co.uk/kenzo">Kenzo</a></li>
                <li><a href="http://www.milanstyle.co.uk/kris-van-assche">KRIS VAN ASSCHE</a></li>
                <li><a href="http://www.milanstyle.co.uk/lanvin">Lanvin</a></li>
                <li><a href="http://www.milanstyle.co.uk/maison-martin-margiela">Maison Martin Margiela</a></li>
                <li><a href="http://www.milanstyle.co.uk/marc-by-marc-jacobs">Marc by Marc Jacobs</a></li>
                <li><a href="http://www.milanstyle.co.uk/missoni">Missoni</a></li>
                <li><a href="http://www.milanstyle.co.uk/moncler">Moncler</a></li>
                <li><a href="http://www.milanstyle.co.uk/neil-barrett">Neil Barrett</a></li></ul>
            <ul><li><a href="http://www.milanstyle.co.uk/paul-smith">Paul Smith</a></li>
                <li><a href="http://www.milanstyle.co.uk/philipp-plein">PHILIPP PLEIN</a></li>
                <li><a href="http://www.milanstyle.co.uk/raf-simons">Raf Simons</a></li>
                <li><a href="http://www.milanstyle.co.uk/ralph-lauren">Ralph Lauren</a></li>
                <li><a href="http://www.milanstyle.co.uk/rick-owens">Rick Owens</a></li>
                <li><a href="http://www.milanstyle.co.uk/saint-laurent">SAINT LAURENT</a></li>
                <li><a href="http://www.milanstyle.co.uk/salvatore-ferragamo">Salvatore Ferragamo</a></li>
                <li><a href="http://www.milanstyle.co.uk/thom-browne">THOM BROWNE</a></li>
                <li><a href="http://www.milanstyle.co.uk/valentino">Valentino</a></li>
                <li><a href="http://www.milanstyle.co.uk/vivienne-westwood-man">Vivienne Westwood Man</a></li>
                <li><a href="http://www.milanstyle.co.uk/yohji-yamamoto">Yohji Yamamoto</a></li>
            </ul>
            <ul>        </ul>
            <div class="viewall" style="text-align:right; padding-right:55px; clear:both; display:block; float:right">
                <a style="margin-bottom:0px; padding-bottom:0px" href="/designer-directory-a-z/">View all designers/</a></div></div>
    </li>
    <li>
        <a href="#" class="category">CLOTHING &amp; FOOTWEAR</a>
        <div class="sub" style="width: 318px; opacity: 0; display: none;">
            <ul>
                <li><a href="/mens/coats-jackets/">Coats &amp; Jackets</a></li>
                <li><a href="/mens/jeans/">Jeans</a></li>
                <li><a href="/mens/knitwear/">Knitwear</a></li>
                <li><a href="/mens/nightwear/">Nightwear</a></li>
                <li><a href="/mens/polo-shirts/">Polo Shirts</a></li>
                <li><a href="/mens/shirts/">Shirts</a></li>
                <li><a href="/mens/shoes/">Shoes</a></li>
                <li><a href="/mens/shorts/">Shorts</a></li>
                <li><a href="/mens/suits/">Suits &amp; Tailoring</a></li>
                <li><a href="/mens/sunglasses/">Sunglasses</a></li>
                <li><a href="/mens/swimwear/">Swimwear</a></li>
                <li><a href="/mens/t-shirts/">T-Shirts</a></li>
                <li><a href="/mens/tops/">Tops</a></li>
                <li><a href="/mens/trousers/">Trousers</a></li>
                <li><a href="/mens/underwear/">Underwear</a></li>
            </ul>

            <ul>
                <!-- Accesories sub menu -->
                <li><a href="/mens/hats">Hats</a></li>
                <li><a href="/mens/scarves/">Scarves</a></li>
                <li><a href="/mens/belts/">Belts</a></li>
                <li><a href="/mens/cufflinks/">Cufflinks</a></li>
                <li><a href="/mens/wash-bags/">Wash Bags</a></li>
                <li><a href="/mens/ties/">Ties</a></li>
                <li><a href="/mens/wallets/">Wallets</a></li>
                <li><a href="/mens/handkerchiefs/">Handkerchiefs</a></li>
                <li><a href="/mens/socks/">Socks</a></li>
                <li><a href="/mens/umbrellas/">Umbrellas</a></li>
                <li><a href="/mens/gloves/">Gloves</a></li>
                <li><a href="/mens/tech-cases/">Ipod &amp; Ipad Cases</a></li>
                <li><a href="/mens/bags/">Bags</a></li>
                <li><a href="/mens/jewellery/">Jewellery</a></li>
            </ul>
        </div>
    </li>
    <li>
        <a href="http://www.milanstyle.co.uk/private-shopping/index.php" class="styleguides" style="width: 143px;">*PRIVATE SHOPPING</a> 
        <!--div class="sub" style="opacity: 0; display: none; width: 159px;">
            <ul>
                <li><a href="http://www.milanstyle.co.uk/style-guide/ralph-lauren">Ralph Lauren Style Guide</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/dior-homme-jeans">Dior Homme Jeans</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/vivienne-westwood-shirts">Vivienne Westwood Shirts</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/dior-homme-denim-japan">ディオールオム デニム</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/belstaff-jackets">Belstaff Jackets &amp; Bags</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/timeless-style">Timeless Style</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/mens-luxury-brands">Men's Luxury Brands</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/barbour-jackets">Barbour Jackets</a></li>
                <li><a href="http://www.milanstyle.co.uk/style-guide/made-in-england-shoes">Made in England Shoes</a></li>
            </ul>
        </div-->
    </li>
    <li>
        <a href="http://www.milanstyle.co.uk/designer-directory-a-z/" class="directory">DESIGNER A-Z</a>        </li>
    <li>
        <a href="http://www.milanstyle.co.uk/review/" class="reviews">REVIEWS</a>
        <div class="sub" style="margin-left: -315px; width: 477px; opacity: 0; display: none;">
            <ul>
                <li><a href="http://www.milanstyle.co.uk/review/belstaff">BELSTAFF</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/boutique1-com">Boutique1.com</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/burberry-com">BURBERRY.com</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/farfetch-com">FarFetch.com</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/gucci">Gucci</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/harrods">Harrods</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/house-of-fraser">House of Fraser</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/j-crew">J.Crew</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/jules-b">Jules B</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/lane-crawford">Lane Crawford</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/ln-cc">LN-CC</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/luisaviaroma">Luisaviaroma</a></li>
            </ul>
            <ul>
                <li><a href="http://www.milanstyle.co.uk/review/luisaviaroma-japan">ルイザヴィアローマ</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/matches-fashion">MATCHES FASHION</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/mr-porter">MR PORTER</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/my-wardrobe-com">my-wardrobe.com</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/my-wardrobe-japan">私のワードローブ</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/oki-ni">OKI-NI</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/pilot-clothing">Pilot Clothing</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/psyche">Psyche</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/raffaello-network">Raffaello Network</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/ralph-lauren">Ralph Lauren</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/selfridges">Selfridges</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/ssense">SSENSE</a></li>
            </ul>
            <ul>
                <li><a href="http://www.milanstyle.co.uk/review/stylebop">STYLEBOP</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/thecorner-com">thecorner.com</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/triads">Triads</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/van-mildert">Van Mildert</a></li>
                <li><a href="http://www.milanstyle.co.uk/review/woodhouse-clothing">Woodhouse Clothing</a></li>
            </ul>
        </div>
    </li>
    <li>
        <a href="http://www.milanstyle.co.uk/blog/" class="blogs">BLOG</a>        </li>
    <li>
        <a href="http://www.milanstyle.co.uk/contact-us/" class="contact">CONTACT US</a>        </li>
</ul>
<div class="cls"></div>
</div>



<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mymenu.js"></script>


<div id="breadcrumb">

</div>

<div id="breadcrumb">

</div>

</div><!-- #header -->
<div id="main">
<div id="container">
<div id="home-content">
<div id="content_register">

        <?php echo $content; ?>


</div>
</div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try{
var pageTracker = _gat._getTracker("UA-13017310-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>