<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/backend.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/backend/form.css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div class="container-fluid" id="page">
    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav sidebar-nav-fixed">
                <div class="sidebar">
                    <?php
                    $this->widget('bootstrap.widgets.TbMenu', array(
                        'type' => 'list',
                        'items' => array(
                            array('label' => 'Admin Panel', 'itemOptions' => array('class' => 'nav-header')),
                            '',
                            array('label' => 'Step 1', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'Popular Brands', 'url' => array('/brands/popular')),
                            array('label' => 'All Brands', 'url' => array('/brands/index')),

                            array('label' => 'Step 2', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'Public Categories', 'url' => array('/wpTerms/publick')),
                            array('label' => 'All Categories', 'url' => array('/wpTerms/index')),

                            array('label' => 'Step 3', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'Size Categories', 'url' => array('/categoriSizes/admin')),
                            array('label' => 'Create Size Category ', 'url' => array('/categoriSizes/create')),
                            '',
                            array('label' => 'Size types', 'url' => array('/size/admin')),
                            array('label' => 'Create Size type', 'url' => array('/size/create')),
                            '',
                            array('label' => 'Size Relations', 'url' => array('/productsProperties/admin')),
                            '',
                            array('label' => 'Users', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'List User', 'url' => array('/users/admin')),
                            /*
                            array('label' => 'Group Properties Products', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'List Properties Group', 'url' => array('/propertyCategory/admin')),
                            array('label' => 'Create Properties Group', 'url' => array('/propertyCategory/create')),
                            array('label' => 'Properties', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => 'List Properties', 'url' => array('/properties/admin')),
                            array('label' => 'Create Properties', 'url' => array('/properties/create')),
                            */
                            '',
                            array('label' => 'Log out', 'url' => array('site/logout')),
                        )
                    ));
                    ?>
                </div>
            </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                        ));
                        ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>
        </div>



        <div class="clear"></div>

        <div id="footer">

        </div><!-- footer -->
    </div>
</div><!-- page -->

</body>
</html>
