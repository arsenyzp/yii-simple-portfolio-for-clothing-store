<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'properties-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownList($model,'id_catigories', CHtml::listData(PropertyCategory::model()->findAll(), 'id', 'name'), array('empty'=>'--Select group--')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'description',array('class'=>'span5','maxlength'=>1024)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
