<?php
$this->breadcrumbs=array(
	'Properties',
);

$this->menu=array(
	array('label'=>'Create Properties','url'=>array('create')),
	array('label'=>'Manage Properties','url'=>array('admin')),
);
?>

<h1>Properties</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
