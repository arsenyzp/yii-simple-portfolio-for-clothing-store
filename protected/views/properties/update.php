<?php
$this->breadcrumbs=array(
	'Properties'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Properties','url'=>array('index')),
	array('label'=>'Create Properties','url'=>array('create')),
	array('label'=>'View Properties','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Properties','url'=>array('admin')),
);
?>

<h1>Update Properties <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>