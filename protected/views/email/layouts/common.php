<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: 'Times New Roman' !important;">
<table  align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:#cccccc 1px solid; display: block; width: 660px">
    <tr style="height:30px;display: block; ">
        <td style="height:30px;display: block; "></td>
        <td style="height:30px;"></td>
        <td style="height:30px;"></td>
    </tr>
    <tr><td style="width: 30px"></td>
        <td>
            <table width="600" align="center" cellpadding="0" cellspacing="0">

            <?php echo $content; ?>

                <tr style="height: 20px; display: block;">
                    <td style="height: 20px;"></td>
                </tr>
                <tr>
                    <td border="0" style="border:none;">
                        <table style="height: 270px; background: #f8f8f8; width: 100%;">
                            <tr>
                                <td></td>
                                <td>
                                    <center>
                                        <table style="text-align: center; font-size: 20px; font-family: 'Times New Roman' !important; font-style: italic; ">
                                            <tr>
                                                <td>

                                                    <font face='Times New Roman' color="#3d3d3d">QUESTIONS ABOUT ANY OF THESE ITEMS?</font>

                                                </td>
                                            </tr>

                                            <tr style="height: 15px; display: block;">
                                                <td style="height: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <font face='Times New Roman' color="#3d3d3d">Contact your Milan Style Private Shopping Assistant at the email addresses below:</font>

                                                </td>
                                            </tr>

                                            <tr style="height: 15px; display: block;">
                                                <td style="height: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <font face='Times New Roman' color="#3d3d3d">For items from Luisaviaroma.com: <b><font face='Times New Roman' color="#3d3d3d">customercare@luisaviaroma.com</font></b><br />
                                                        For items from other stores, please refer to the store&#180;s Customer Service page.</font>
                                                </td>
                                            </tr>
                                            <tr style="height: 15px; display: block;">
                                                <td style="height: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <font face='Times New Roman' color="#3d3d3d">The Private Shopping Experience Continues with These<br />
                                                                                                Exclusive Offers for Milan Style Members:</font>
                                                </td>
                                            </tr>
                                        </table>
                                    </center>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr style="height: 20px; display: block;">
                    <td style="height: 20px;"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 20px; font-family: 'Times New Roman'; width: 100%; color: #FFF; display: block;">
                        <center style="padding-top: 65px;">
                            <a href="http://www.jdoqocy.com/click-3824485-10917875?SID=EMAIL-PS-Privelege&url=http%3A%2F%2Fwww.luisaviaroma.com%2Flanding.aspx%3Fpage%3Dprivilege-card%26cnt%3DMILAN10%26utm_source%3DMilanstyle%26utm_medium%3DPrivilege%26utm_campaign%3D58I%2BNew%2BCollections" target="_blanck">
                                <img src="<?php Yii::app()->request->baseUrl; ?>http://milanstyle.co.uk/emailim/banner.jpg" alt="MilanStyle" />
                            </a>
                        </center>
                    </td>
                </tr>

                <tr style="width: 100%; height: 40px; display: block; ">
                    <td style="width: 100%; height: 40px;"></td>
                </tr>
                <tr style="width: 100%; height: 3px; background: #000; display: block;">
                    <td style="width: 100%; height: 3px; background: #000;"></td>
                </tr>
                <tr style="width: 550px; height: 40px; text-align:right; padding-right: 10px;display: block;">
                    <td style="width: 550px; height: 40px; text-align:right; padding-right: 10px;display: block; padding-top: 10px;">
                        <a href="#">
                            <img src="<?php Yii::app()->request->baseUrl; ?>http://milanstyle.co.uk/emailim/soc.png" alt="" />
                        </a>
                    </td>
                </tr>
                <tr style="width: 100%; height: 3px; background: #000; display: block;">
                    <td style="width: 100%; height: 3px; background: #000;"></td>
                </tr>
                <tr style="width: 100%; height: 10px; display: block;">
                    <td style="width: 100%; height: 10px; display: block;"></td>
                </tr>
                <tr>
                    <td style="text-align: center; color: #c6c6c6; font-family: 'Times New Roman'; font-style: regular; width:100%; ">
                        <center>
                            COPYRIGHT &#169; 2012 MILANSTYLE.CO.UK<br />
                        227 MARINER'S HOUSE, 67 NORFOLK ST, L1 0BG
                        </center>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 30px"></td>
    </tr>
    <tr style="height:30px; display: block;">
        <td style="height:30px;"></td>
        <td style="height:30px;"></td>
        <td style="height:30px;"></td>
    </tr>
</table>
</body>
</html>