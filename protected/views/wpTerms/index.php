<?php
$this->breadcrumbs=array(
    ' Brand'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List WpFpBrandMapping','url'=>array('index')),
    array('label'=>'Create WpFpBrandMapping','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wp-fp-brand-mapping-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

    <h1>Manage Public Categories</h1>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'publick-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'category-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->publick(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'term_id',
            'type' => 'raw',
            'value' => '$data->term_id',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => '$data->name',
        ),
        array(
            'name' => 'publick',
            'type' => 'raw',
            'value' => 'CHtml::checkBox($data->term_id, $data->isPublick(), array("uncheckValue"=>0))',
            //'filter' => PopularBrandso::model()->getStatusList()
        ),
    ),
));
?>
    <input type="hidden" value="Popular" name="publick" />
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Save')); ?>
<?php $this->endWidget(); ?>