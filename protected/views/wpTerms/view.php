<?php
$this->breadcrumbs=array(
	'Wp Terms'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List WpTerms','url'=>array('index')),
	array('label'=>'Create WpTerms','url'=>array('create')),
	array('label'=>'Update WpTerms','url'=>array('update','id'=>$model->term_id)),
	array('label'=>'Delete WpTerms','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->term_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WpTerms','url'=>array('admin')),
);
?>

<h1>View WpTerms #<?php echo $model->term_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'term_id',
		'name',
		'slug',
		'term_group',
	),
)); ?>
