<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle="Private Shopping: Step 2, Tell Us Your Clothing and Shoe Size";
$this->breadcrumbs=array(
    'Step 2',
);
?>

<div id="title_step">
    <div id="name_step">Step 2. Select your favourite brands</div>
    <div id="count_step">2/3</div>
    <div style="clear: both;"></div>
</div>
<div id="title_two">
    <div id="title_category">
        Categories
    </div>
    <div id="title_size">
        Sizes
    </div>
    <div style="clear: both;"></div>
</div>

<form method="POST" id="step-form">
<div id="select_preference_catigories">
    <?php
    /*$form=$this->beginWidget('CActiveForm', array(
        'id'=>'step-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    ));*/
     ?>

    <?php  foreach($list as $k=>$item){ ?>

        <label class="row_option" for="<?php echo $k; ?>">
            <div class="checkbox_option">
                <input name="category_<?php echo $item->term_id; ?>" <?php echo isset($selected[$item->term_id]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="<?php echo $k; ?>">
            </div>
            <?php echo $item->name; ?>
        </label>

    <?php } ?>

    <input type="hidden" value="brands" name="step" />
    <?php
    /* $this->endWidget();*/
    ?>
</div>
<div id="group_select">
    <?php
    $i = 0;
     foreach($listSize as $k=>$item){ 
    $i++;
    ?>
  <fieldset id="step_2">
            <legend class="titel_milan"><?php echo $k; ?></legend>
    <?php if($i == 1){ ?>
      
            <table>
                <tr>
                    <td>
                        <table>
                            <tr><td>UK/US</td></tr>
                            <tr><td>Italian/EU</td></tr>
                            <tr><td></td></tr>
                        </table>
                    </td>
                    <?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']);
                    $size_l = $tmp_s[1];
                    $size_l = explode('/',$size_l);                 
                      ?>
                    <td>
                        <table id="table_<?php echo $field['id'];?>">
                            <tr><td><?php echo $size_l[0]; ?></td></tr>
                            <tr><td><?php echo $size_l[1]; ?></td></tr>
                            <tr><td>
                                <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="size_<?php echo $field['id'];?>">
                            </td></tr>
                        </table>
                    </td>
                    <?php }?>
                </tr>
            </table> 
        <?php }?>

        <?php if($i == 2){ ?>
              <table>
                <tr>
                    <td>
                        <table>
                            <tr><td>UK/US</td></tr>
                            <tr><td>Italy</td></tr>
                            <tr><td></td></tr>
                        </table>
                    </td>
                    <?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']);
                    $size_l = $tmp_s[1];
                    $size_l = explode('/',$size_l);                 
                      ?>
                    <td>
                        <table id="table_<?php echo $field['id'];?>">
                            <tr><td><?php echo $size_l[0]; ?></td></tr>
                            <tr><td><?php echo $size_l[1]; ?></td></tr>
                            <tr><td>
                                <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="size_<?php echo $field['id'];?>">
                            </td></tr>
                        </table>
                    </td>
                    <?php }?>
                </tr>
            </table> 
        <?php }?>

        <?php if($i == 3){ ?>
          <table>
                <tr>
                    <td>
                        <table>
                            <tr><td>UK / US / AUS</td></tr>
                            <tr><td>Italian / EU / Japan</td></tr>
                            <tr><td></td></tr>
                        </table>
                    </td>
                    <?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']);
                    $size_l = $tmp_s[1];
                    $size_l = explode('/',$size_l);                 
                      ?>
                    <td>
                        <table id="table_<?php echo $field['id'];?>">
                            <tr><td><?php echo $size_l[0]; ?></td></tr>
                            <tr><td><?php echo $size_l[1]; ?></td></tr>
                            <tr><td>
                                <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="size_<?php echo $field['id'];?>">
                            </td></tr>
                        </table>
                    </td>
                    <?php }?>
                </tr>
            </table> 
        <?php }?>

          <?php if($i == 5){ ?>
             <table>
                <tr>
                    <td>
                        <table>
                            <tr><td></td></tr>
                             <tr><td></td></tr>
                        </table>
                    </td>
                    <?php foreach($item as $field){?>

                    <td>
                        <table id="table_<?php echo $field['id'];?>">
                            <tr><td><?php echo $field['name']; ?></td></tr>
                            <tr><td style="text-align: center;">
                                <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="size_<?php echo $field['id'];?>">
                            </td></tr>
                        </table>
                    </td>
                    <?php }?>
                </tr>
            </table> 
        
        <?php }?>

        <?php if($i == 4){ ?>
             <table>
                <tr>
                    <td>
                        <table>
                            <tr><td>UK</td></tr>
                            <tr><td>US</td></tr>
                            <tr><td>Italian / EU</td></tr>
                            <tr><td></td></tr>
                        </table>
                    </td>
                    <?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']);
                    $size_l = $tmp_s[1];
                    $size_l = explode('/',$size_l);                 
                      ?>
                    <td>
                        <table id="table_<?php echo $field['id'];?>">
                            <tr><td><?php echo $size_l[0]; ?></td></tr>
                            <tr><td><?php echo $size_l[1]; ?></td></tr>
                            <tr><td><?php echo $size_l[2]; ?></td></tr>
                            <tr><td>
                                <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option2" id="size_<?php echo $field['id'];?>">
                            </td></tr>
                        </table>
                    </td>
                    <?php }?>
                </tr>
            </table> 
        <?php }?>

      
</fieldset>
    <?php } ?>
</div>
</form>
<div class="div_button_option right">
    <a href="<?php echo $this->createAbsoluteUrl('register/step1');  ?>" id="previos" class="button_option">PREVIOUS</a>
    <a href="#" id="next_click" class="button_option">NEXT</a>
</div>
<div style="height: 50px"></div>
<script>
    $j(document).ready(function(){
        $j('#next_click').on('click', function(){
            $j('#step-form').submit();
        });

        $j('.check_option2').on('click', function(){
            var id = $j(this).attr('id');
            id = id.replace('size_', '');
            //s.replace(/old/g, 'new');
            if($j(this).attr('checked') == 'checked')
                $j('#table_'+id).css('background', '#F1F1F1');
            else
                $j('#table_'+id).css('background', 'none');
        });
test_check2();
    });



    function test_check2(){
    $j('.check_option2').each(function(){
         var id = $j(this).attr('id');
            id = id.replace('size_', '');
        if($j(this).attr('checked') == 'checked')
                $j('#table_'+id).css('background', '#F1F1F1');
            else
                $j('#table_'+id).css('background', 'none');
    });
}
</script>