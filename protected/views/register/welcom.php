<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle="Welcome To Milan Style Private Shopping ".strtoupper($username);
$this->breadcrumbs=array(
    'Welcome',
);
?>

<div id="wellcomus">
    <div id="title_welcom">
        WELCOME <?php echo strtoupper($username); ?>
    </div>
    <div id="title_welcom2">
        to Milan Style Private Shopping
    </div>
    <div id="content_welcom">
        <p>
            As a taster, an email of any matches over the last 7 days has just been sent to you.
        </p>
        <p>
            Important: Please check that this email has not gone into spam or junk and add privateshopping@milanstyle.co.uk <br />
            to your contact's list, or mark as safe to make sure you receive our emails.
        </p>
        <p>
            If you use GMAIL, you may find our email in your ‘Promotions’ tab. To make sure<br /> you do not miss out on our emails, simply drag any ‘MILAN STYLE’ email into your<br /> ‘Primary’ inbox and click the ‘Yes’ button when asked to confirm.
        </p>    
        <p>
            Ongoing matches will be EMAILED to you at intervals according to your selected days for receiving emails.
        </p>
        <p>
            The online SHOP section displays matches of your favourite brands in your latest email and all past emails.
        </p>
        <div style="border:none; text-align: center; width: 700px;" class="select_box">
             <?php echo CHtml::link('View my *Private Shopping profile', array('cabinet/index/user/'.$id_us), array('class'=>'cab_button')); ?>
        </div>
    </div>
</div>