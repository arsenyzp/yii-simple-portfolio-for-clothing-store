<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle="Private Shopping: Complete Your Registration";
$this->breadcrumbs=array(
    'Step 3',
);
?>
<div id="title_step">
    <div id="name_step">Step 2. Finish & receive alerts</div>
    <div id="count_step">2/2</div>
    <div style="clear: both;"></div>
</div>
<div id="title_two">
    <div id="title_category">
        Update Days
    </div>
    <div id="title_size">
        Details
    </div>
</div>
<div id="select_preference_catigories">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'register-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
    )); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->error($model,'day[]'); ?>
    <input type="hidden" name="facebook" id="finp">
    <label class="row_option" for="1"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>1, 'value'=>1, 'checked'=>(isset($selected[1]) && $selected[1] == 1 ?'checked':''))); ?>
        </div>Monday</label>
    <label class="row_option" for="2"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>2, 'value'=>5, 'checked'=>(isset($selected[5]) && $selected[5] == 5 ?'checked':''))); ?>
        </div>Friday</label>
    <label class="row_option" for="3"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>3, 'value'=>2, 'checked'=>(isset($selected[2]) && $selected[2] == 2 ?'checked':''))); ?>
        </div>Tuesday</label>
    <label class="row_option" for="4"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>4, 'value'=>6, 'checked'=>(isset($selected[6]) && $selected[6] == 6 ?'checked':''))); ?>
        </div>Saturday</label>
    <label class="row_option" for="5"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>5, 'value'=>3, 'checked'=>(isset($selected[3]) && $selected[3] == 3 ?'checked':''))); ?>
    </div>Wednesday</label>
    <label class="row_option" for="6"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>6, 'value'=>7, 'checked'=>(isset($selected[7]) && $selected[7] == 7 ?'checked':''))); ?>
    </div>Sunday</label>
    <label class="row_option" for="7"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>7, 'value'=>4, 'checked'=>(isset($selected[4]) && $selected[4] == 4 ?'checked':''))); ?>
    </div>Thursday</label>
    <input type="hidden" name="days" value="days" />

</div>

<div id="group_select">
    <div id="facebook_login">
        <?php
         //echo CHtml::link('<img src="'.Yii::app()->request->baseUrl.'/images/facebook.png" alt="facebook" />', Facebook::getUrl());
        ?>
        <a href="<?php echo Facebook::getUrl(); ?>"  id="facebook_click" onclick="return false;">
          <?php echo '<img src="'.Yii::app()->request->baseUrl.'/images/fb_register_button.png" alt="facebook" />';?>
        </a>
    </div>
    <div id="or">Or</div>
    <div id="login_form">
        <div>
            <?php echo $form->textField($model,'firstname',array('placeholder'=>'First Name')); ?>
            <?php echo $form->error($model,'firstname'); ?>
        </div>
        <div>
            <?php echo $form->textField($model,'lastname',array('placeholder'=>'Last Name')); ?>
            <?php echo $form->error($model,'lastname'); ?>
        </div>
        <div>
            <?php echo $form->textField($model,'email',array('placeholder'=>'Email Address')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
        <div id="small_text">
            By clicking REGISTER you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy and Cookie Policy</a> and consent to receive email from MilanStyle.co.uk
        </div>
        <?php $this->endWidget();?>
    </div>
</div>
<div class="div_button_option right">
    <a href="<?php echo $this->createAbsoluteUrl('register/step1');  ?>" id="previos" class="button_option">PREVIOUS</a>
    <a href="#" id="next_click" class="button_option">NEXT</a>
</div>
<input type="hidden" value="brands" name="step" />
<div style="height: 50px"></div>
<script>
    $j(document).ready(function(){
        $j('#next_click').on('click', function(){
            $j('#register-form').submit();
        });

        $j('#facebook_click').on('click', function(){
            var t = 0;
            $j('.check_option').each(function(){
                if($j(this).attr('checked') == 'checked'){
                   t++;
                }
            });
            if(t == 0){
                alert('Select day!');
                return false;
            }
            window.open(this.href, "Facebook", "width=1000,height=500,resizable=yes,scrollbars=no,status=no, top=0, left=200");
            $j('#finp').val('facebook');
            $j('#register-form').submit();
        });
    });
</script>