<?php
$this->breadcrumbs=array(
	'Wp Fp Brand Mappings'=>array('index'),
	$model->brand_map_id,
);

$this->menu=array(
	array('label'=>'List WpFpBrandMapping','url'=>array('index')),
	array('label'=>'Create WpFpBrandMapping','url'=>array('create')),
	array('label'=>'Update WpFpBrandMapping','url'=>array('update','id'=>$model->brand_map_id)),
	array('label'=>'Delete WpFpBrandMapping','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->brand_map_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WpFpBrandMapping','url'=>array('admin')),
);
?>

<h1>View WpFpBrandMapping #<?php echo $model->brand_map_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'brand_map_id',
		'brand_name',
		'wp_brand_name',
	),
)); ?>
