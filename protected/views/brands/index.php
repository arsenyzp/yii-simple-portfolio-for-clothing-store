<?php
$this->breadcrumbs=array(
    ' Brand'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List WpFpBrandMapping','url'=>array('index')),
    array('label'=>'Create WpFpBrandMapping','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wp-fp-brand-mapping-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

    <h1>Manage Popular Brands</h1>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'popular-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'category-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->popular(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'type' => 'raw',
            'value' => '$data->id',
        ),
        array(
            'name' => 'brands',
            'type' => 'raw',
            'value' => '$data->brands',
        ),
        array(
            'name' => 'popular',
            'type' => 'raw',
            'value' => 'CHtml::checkBox($data->id, $data->isPopular(), array("uncheckValue"=>0))',
            //'filter' => PopularBrandso::model()->getStatusList()
        ),
    ),
));
?>
    <input type="hidden" value="Popular" name="popular" />
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Save')); ?>
<?php $this->endWidget(); ?>