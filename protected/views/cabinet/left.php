<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.assets.js.cabinet').'/left.js'
    ),
    CClientScript::POS_END
); 
?>
<div class="cabinet_left_title" id="your_brands" style="cursor: pointer;">YOUR BRANDS</div>
<div id="your_brands_list">
	<?php foreach ($user_brands as $brand) { ?>
		<div class="cabinet_left_list"><?php echo $brand->p->brands; ?></div>
	<?php } ?>
</div>
<div><a href="<?php echo $this->createUrl('cabinet/brands', array('user'=>$user_id)); ?>" class="cabinet_left_a">Add to or amend your brands</a></div>
<div class="cabinet_height10"></div>
<!--
<?php
/*
<div class="cabinet_left_title" id="your_size" style="cursor: pointer;">YOUR SIZES</div>
<div id="your_size_list">
	<?php/*

	$i = 0;
	foreach($listSize as $k=>$item){ 
		$i++; 
		?>
		<div class="cabinet_left_list" style="color: #222222;"><?php echo $k; ?></div>
		<?php if($i == 1){ ?>
			<?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']); 

                    if(!in_array($field['id'], $selected_size))
                    	continue;
                      ?>
						<div class="cabinet_left_list"><?php echo $tmp_s[1]; ?></div>
			<?php }?>				
		<?php }?>
		<?php if($i == 2){ ?>
			<?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']); 
                    if(!in_array($field['id'], $selected_size))
                    	continue;
                      ?>
						<div class="cabinet_left_list"><?php echo $tmp_s[1]; ?></div>
			<?php }?>
		<?php }?>
		<?php if($i == 3){ ?>
			<?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']); 
                    if(!in_array($field['id'], $selected_size))
                    	continue;
                      ?>
						<div class="cabinet_left_list"><?php echo $tmp_s[1]; ?></div>
			<?php }?>
		<?php }?>
		<?php if($i == 4){ ?>
			<?php foreach($item as $field){?>
                    <?php
                    $tmp_s = explode(':', $field['name']); 
                    if(!in_array($field['id'], $selected_size))
                    	continue;
                      ?>
						<div class="cabinet_left_list"><?php echo $tmp_s[1]; ?></div>
			<?php }?>
		<?php }?>
		<?php if($i == 5){ ?>
			<?php foreach($item as $field){
				if(!in_array($field['id'], $selected_size))
                    	continue;
				?>
						<div class="cabinet_left_list"><?php echo $field['name']; ?></div>
			<?php }?>
		<?php }?>
	<?php }*/ ?>
</div>
<div><a href="<?php echo $this->createUrl('cabinet/sizes', array('user'=>$user_id)); ?>" class="cabinet_left_a">Add to or amend your size</a></div>
*/
?>
-->
<div class="cabinet_height10"></div>
<div class="cabinet_left_title">VIEW BY DISCOUNT</div>
<div class="cabinet_height10"></div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="sale_any" value="1" <?php echo (isset($sale_preference[1]) && $sale_preference[1]>0) ? 'checked="checked"' : '' ?> class="sale_check" />
	<label for="sale_any">Any</label>
</div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="sale_25" id="sale_25" value="2" <?php echo (isset($sale_preference[2]) && $sale_preference[2]>0) ? 'checked="checked"' : '' ?>  class="sale_check" />
	<label for="sale_25">Up to 25% off</label>
</div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="sale_2530" id="sale_2530" value="3" <?php echo (isset($sale_preference[3]) && $sale_preference[3]>0) ? 'checked="checked"' : '' ?>  class="sale_check" />
	<label for="sale_2530">25-50% off</label>
</div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="sale_5075" value="4" <?php echo (isset($sale_preference[4]) && $sale_preference[4]>0) ? 'checked="checked"' : '' ?>  class="sale_check" />
	<label for="sale_5075">50-75% off</label>
</div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="sale_75" value="5" <?php echo (isset($sale_preference[5]) && $sale_preference[5]>0) ? 'checked="checked"' : '' ?>  class="sale_check" />
	<label for="sale_75">More then 75% off</label>
</div>
<div class="cabinet_height10"></div>

<div class="cabinet_left_title">VIEW BY CATEGORY</div>
<div class="cabinet_height10"></div>
<div class="cabinet_left_checkbox">
	<input type="checkbox" name="size" id="select_all_categories" />
	<label for="size">All</label>
</div>

<?php foreach ($categories as $category) { ?>
	<div class="cabinet_left_checkbox">
		<?php echo CHtml::CheckBox('category_'.$category->term_id, in_array($category->term_id, $user_categories), array('value'=>$category->term_id, 'class'=>'check_cat')); ?>
		<?php /*<input type="checkbox" name="category_<?php echo $category->term_id; ?>" <?php echo in_array($category->term_id, $user_categories)?'checked="checked"':''; ?> class='check_cat' />*/?>
		<label for="category_<?php echo $category->term_id; ?>"><?php echo $category->name; ?></label>
	</div>
<?php } ?>

<!--div><a href="<?php echo $this->createUrl('cabinet/categories', array('user'=>$user_id)); ?>" class="cabinet_left_a">Add to or amend your categories</a></div-->