<?php
/* @var $this CabinetController */

$this->breadcrumbs=array(
	'Cabinet',
);
$this->pageTitle='PRIVATE SHOPPING';

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.assets.js.cabinet').'/index.js'
    ),
    CClientScript::POS_END
);
?>
<script type="text/javascript">
  var ajax_url = '<?php echo $this->createAbsoluteUrl('cabinet/ajax', array('user'=>$user_id)); ?>';
</script>
<div>
  <div class="cabinet_top_menu">
    <div id="user_name">Welcome <?php echo $user_name; ?></div>
  </div>
  <div class="cabinet_top_menu right">
    <?php echo CHtml::link('Update My Email Frequency', array('cabinet/days/user/'.$user_id)); ?> - 
    <?php echo CHtml::link('Delete My Account', array('cabinet/delete/user/'.$user_id)); ?>
  </div>
</div>
<div id="left_menu">
    <?php 
      	$this->renderPartial('left', array(
            'user_name'=>$user_name,
            'user_id'=>$user_id, 
            'user_brands'=>$user_brands, 
            'user_categories'=>$user_categories,
            'listSize'=>$listSize,
            'selected_size'=>$selected_size,
            'categories'=>$categories,
            'sale_preference'=>$sale_preference,
             )); 
    ?>
</div>
<div id="product_preference_list">
<?php 
    $this->renderPartial('products', array(
        'list_product'=>$list_product, 
         )); 
?>
</div>
<div style="border:none; text-align: center; width: 700px; margin-left: 235px;" class="select_box">
  <a id="next_page" href="javascript:" class="button_option">Show more matching products</a>  
</div>
<div id="wait_img">
   <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/wait.gif" />
</div>
<div style="height: 50px;"></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>