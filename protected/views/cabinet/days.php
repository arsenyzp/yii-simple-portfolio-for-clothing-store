<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle='PRIVATE SHOPPING - Select your favourite  days';
$this->breadcrumbs=array(
    'Step 2',
);
?>
<div id="title_two">
    <div id="title_category">
        Update Days
    </div>
</div>
<div id="select_preference_catigories" style="width: 315px;">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'register-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>false,
        ),
    )); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->error($model,'day[]'); ?>
    <input type="hidden" name="facebook" id="finp">
    <label class="row_option" for="1"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>1, 'value'=>1, 'checked'=>(isset($selected[1]) && $selected[1] == 1 ?'checked':''))); ?>
        </div>Monday</label>
    <label class="row_option" for="2"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>2, 'value'=>5, 'checked'=>(isset($selected[5]) && $selected[5] == 5 ?'checked':''))); ?>
        </div>Friday</label>
    <label class="row_option" for="3"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>3, 'value'=>2, 'checked'=>(isset($selected[2]) && $selected[2] == 2 ?'checked':''))); ?>
        </div>Tuesday</label>
    <label class="row_option" for="4"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>4, 'value'=>6, 'checked'=>(isset($selected[6]) && $selected[6] == 6 ?'checked':''))); ?>
        </div>Saturday</label>
    <label class="row_option" for="5"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>5, 'value'=>3, 'checked'=>(isset($selected[3]) && $selected[3] == 3 ?'checked':''))); ?>
    </div>Wednesday</label>
    <label class="row_option" for="6"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>6, 'value'=>7, 'checked'=>(isset($selected[7]) && $selected[7] == 7 ?'checked':''))); ?>
    </div>Sunday</label>
    <label class="row_option" for="7"><div class="checkbox_option">
            <?php echo $form->checkBox($model,'day[]',array('class'=>'check_option','id'=>7, 'value'=>4, 'checked'=>(isset($selected[4]) && $selected[4] == 4 ?'checked':''))); ?>
    </div>Thursday</label>
    <input type="hidden" name="days" value="days" />

</div>

 <?php $this->endWidget();?>

<div class="div_button_option right">
    <a href="#" id="next_click" class="button_option">SAVE</a>
</div>
<div style="height: 50px"></div>
<script>
    $j(document).ready(function(){
        $j('#next_click').on('click', function(){
            $j('#register-form').submit();
        });
    });
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>