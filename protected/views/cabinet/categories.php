<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle='PRIVATE SHOPPING - Select your favourite  categories';
$this->breadcrumbs=array(
    'Step 2',
);
?>

<div id="title_step">
    <div id="name_step">Select your favourite categories</div>
    <div id="count_step"></div>
    <div style="clear: both;"></div>
</div>
<div id="title_two">
    <div id="title_category">
        Categories
    </div>
    <div id="title_size">

    </div>
    <div style="clear: both;"></div>
</div>

<form method="POST" id="step-form">
<div id="select_preference_catigories">

    <?php  foreach($list as $k=>$item){ ?>

        <label class="row_option" for="<?php echo $k; ?>">
            <div class="checkbox_option">
                <input name="category_<?php echo $item->term_id; ?>" <?php echo in_array($item->term_id, $selected) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option" id="<?php echo $k; ?>">
            </div>
            <?php echo $item->name; ?>
        </label>

    <?php } ?>

    <input type="hidden" value="brands" name="step" />
    <?php

    ?>
</div>

<div id="group_select">
    <?php 
    /*foreach($listSize as $k=>$item){ ?>
        <fieldset>
            <legend><?php echo $k; ?></legend>
            <?php foreach($item as $field){?>
                <label class="row_option" for="size_<?php echo $field['id'];?>">
                    <div class="checkbox_option">
                        <input name="size_<?php echo $field['id'];?>" <?php echo isset($selected_size[$field['id']]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option" id="size_<?php echo $field['id'];?>">
                    </div>
                    <?php echo $field['name'];?>
                </label>
            <?php }?>
        </fieldset>
    <?php }
    */
     ?>
</div>
</form>
<div class="div_button_option right">
    <a href="#" id="next_click" class="button_option">SAVE</a>
</div>
<div style="height: 50px"></div>
<script>
    $j(document).ready(function(){
        $j('#next_click').on('click', function(){
            $j('#step-form').submit();
        });
    });
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>