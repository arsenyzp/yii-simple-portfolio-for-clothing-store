<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle='PRIVATE SHOPPING - Select your favourite brands';
$this->breadcrumbs=array(
    'Select your favourite brands',
);
?>

<div id="title_step">
    <div id="name_step">Select your favourite brands</div>
    <div id="count_step"></div>
</div>
<div class="div_button_option">
    <a href="<?php echo $this->createAbsoluteUrl('cabinet/brands/user/'.$user_id);  ?>" id="most_popular" class="button_option">MOST POPULAR</a>
    <a href="<?php echo $this->createAbsoluteUrl('cabinet/brands/user/'.$user_id.'/all');  ?>" id="all_brands" class="button_option">ALL BRANDS A-Z</a>
    <div style="clear: both;"></div>
</div>
<div id="select_preference">
    <div style="background: #F1F1F1;">
        <div class="colum_preference" id="whiteblock">
                <?php  $form=$this->beginWidget('CActiveForm', array(
            'id'=>'brands-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>
<div class="col_brands">
    <?php
        $amount = count($list);//23
        $rows = round($amount/4, 0);//5
        if($amount%4 > 0)
           $rows += round(($amount%4)/4,0);
        $i = 0;
        foreach($list as $k=>$item){ 
            if($i%$rows==0 && $i != 0){?>
                </div><div class="col_brands">
              <?php  }?>

                <label class="row_option" style="width: 180px;" for="<?php echo $k; ?>">
                    <div class="checkbox_option">
                         <?php echo CHtml::CheckBox('propert_'.$item->id, in_array($item->id, $selected), array('value'=>$item->id, 'uncheckValue'=>0, 'class'=>'check_option')); ?>
                        <?php /*<input name="propert_<?php echo $item->id; ?>" <?php echo isset($selected[$item->id]) ? 'checked="checked"' : ''; ?> type="checkbox" class="check_option" value="<?php echo $item->id; ?>" id="<?php echo $k; ?>">*/ ?>
                    </div>
                        <?php echo ucfirst(strtolower($item->brands)); ?>
                </label>

    <?php
        $i++;
     }  ?>
</div>
            <input type="hidden" value="brands" name="brands" />
            <?php $this->endWidget();?>
        </div>

        <div id="colum_preference_select">
            <div id="titljslist">SELECTED BRANDS</div>
            <div id="selbrend"></div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<div class="div_button_option right">
    <a href="javascript:" id="select_all" class="button_option">SELECT ALL</a>
    <a href="javascript:" id="deselect_all" class="button_option">DESELECT ALL</a>
    <a href="#" id="next_click" class="button_option">SAVE</a>
</div>
<div style="height: 50px"></div>
<script>
    $j(document).ready(function(){
        $j('#next_click').on('click', function(){
            $j('#brands-form').submit();
        });
    });
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>