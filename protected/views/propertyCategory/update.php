<?php
$this->breadcrumbs=array(
	'Property Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PropertyCategory','url'=>array('index')),
	array('label'=>'Create PropertyCategory','url'=>array('create')),
	array('label'=>'View PropertyCategory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PropertyCategory','url'=>array('admin')),
);
?>

<h1>Update PropertyCategory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>