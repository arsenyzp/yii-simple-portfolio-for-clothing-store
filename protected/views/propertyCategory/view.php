<?php
$this->breadcrumbs=array(
	'Property Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PropertyCategory','url'=>array('index')),
	array('label'=>'Create PropertyCategory','url'=>array('create')),
	array('label'=>'Update PropertyCategory','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PropertyCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PropertyCategory','url'=>array('admin')),
);
?>

<h1>View PropertyCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
