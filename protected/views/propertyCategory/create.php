<?php
$this->breadcrumbs=array(
	'Property Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PropertyCategory','url'=>array('index')),
	array('label'=>'Manage PropertyCategory','url'=>array('admin')),
);
?>

<h1>Create PropertyCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>