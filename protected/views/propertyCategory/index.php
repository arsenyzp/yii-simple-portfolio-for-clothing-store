<?php
$this->breadcrumbs=array(
	'Property Categories',
);

$this->menu=array(
	array('label'=>'Create PropertyCategory','url'=>array('create')),
	array('label'=>'Manage PropertyCategory','url'=>array('admin')),
);
?>

<h1>Property Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
