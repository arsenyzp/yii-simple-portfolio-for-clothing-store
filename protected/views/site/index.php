<?php
/* @var $this SiteController */

//$this->pageTitle=Yii::app()->name;
$this->pageTitle="MILAN STYLE Private Shopping: Sign Up For New Arrivals & Sale Alerts";
$model = Properties::model();
?>

	<div id="start_text">
		<div id="start_title">
			<p>
				<span style="font-size: 44px">3 Reasons</span><span style="font-size: 30px"> to sign up to </span>
			</p>
			<p>
				<span style="font-size: 54px">Private Shopping</span>
			</p>
		</div>
		<div class="start_text">
			<div id="start_step">1.</div>
			Be emailed with NEW ARRIVALS from your favourite luxury brands.<span style="font-style: italic;"></span>
		</div>
		<div class="start_text">
			<div id="start_step">2.</div>
			Find out when styles from <span style="font-style: italic;">your favourite luxury brands</span> go on SALE.
		</div>
		<div class="start_text">
			<div id="start_step">3.</div>
			Get exclusive access to annual <span style="font-style: italic;">Private Sales </span>from some of your favourite online stores,<span style="font-style: italic;"> before anyone else!</span>
		</div>
		<div style="text-align: right;">
			<a href="<?php echo $this->createAbsoluteUrl('register/step1');  ?>" id="next_click" class="button_option">GET STARTED</a>
		</div>
	</div>
	<div id="prev_email">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prev.png" />
	</div>
	<div id="start_grey_line"></div>
	<div id="start_lt">MILANSTYLE<span style="font-style: italic;"> as see in:</span></div>
	<div style="margin-left: 40px;"
		<div class="start_brand_logo">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/style.png" />
		</div>
		<div class="start_brand_logo start_margin">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/mrporter.png" />
		</div>
		<div class="start_brand_logo">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/grazia.png" />
		</div>
	</div>
	<div style="margin-left: 40px;">
		<div class="start_brand_logo_footer">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gq.png" />
		</div>
		<div class="start_brand_logo_footer">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/gq2.png" />
		</div>
	</div>
	<div style="height:50px;"></div>