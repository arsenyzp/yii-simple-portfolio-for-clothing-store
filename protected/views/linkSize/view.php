<?php
$this->breadcrumbs=array(
	'Link Sizes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LinkSize','url'=>array('index')),
	array('label'=>'Create LinkSize','url'=>array('create')),
	array('label'=>'Update LinkSize','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LinkSize','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LinkSize','url'=>array('admin')),
);
?>

<h1>View LinkSize #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'size_id',
		'properties_id',
	),
)); ?>
