<?php
$this->breadcrumbs=array(
	'Link Sizes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LinkSize','url'=>array('index')),
	array('label'=>'Manage LinkSize','url'=>array('admin')),
);
?>

<h1>Create LinkSize</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>