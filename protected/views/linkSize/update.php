<?php
$this->breadcrumbs=array(
	'Link Sizes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LinkSize','url'=>array('index')),
	array('label'=>'Create LinkSize','url'=>array('create')),
	array('label'=>'View LinkSize','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LinkSize','url'=>array('admin')),
);
?>

<h1>Update LinkSize <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>