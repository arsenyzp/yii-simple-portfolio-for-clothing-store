<?php
$this->breadcrumbs=array(
	'Link Sizes',
);

$this->menu=array(
	array('label'=>'Create LinkSize','url'=>array('create')),
	array('label'=>'Manage LinkSize','url'=>array('admin')),
);
?>

<h1>Link Sizes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
