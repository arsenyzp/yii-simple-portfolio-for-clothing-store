<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'size-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'size',array('class'=>'span5','maxlength'=>256)); ?>

    <?php echo $form->dropDownList($model,'category_id', CHtml::listData(CategoriSizes::model()->findAll(), 'id', 'name'), array('empty'=>'--Select categories--')); ?>

	<?php echo $form->textFieldRow($model,'description',array('class'=>'span5','maxlength'=>512)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
