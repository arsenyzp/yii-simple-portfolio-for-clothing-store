<?php
$this->breadcrumbs=array(
	'Categori Sizes',
);

$this->menu=array(
	array('label'=>'Create CategoriSizes','url'=>array('create')),
	array('label'=>'Manage CategoriSizes','url'=>array('admin')),
);
?>

<h1>Categori Sizes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
