<?php
$this->breadcrumbs=array(
	'Categori Sizes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List CategoriSizes','url'=>array('index')),
	array('label'=>'Create CategoriSizes','url'=>array('create')),
	array('label'=>'Update CategoriSizes','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete CategoriSizes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CategoriSizes','url'=>array('admin')),
);
?>

<h1>View CategoriSizes #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
