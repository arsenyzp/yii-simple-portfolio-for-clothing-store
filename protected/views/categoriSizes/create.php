<?php
$this->breadcrumbs=array(
	'Categori Sizes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CategoriSizes','url'=>array('index')),
	array('label'=>'Manage CategoriSizes','url'=>array('admin')),
);
?>

<h1>Create Category Size</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>