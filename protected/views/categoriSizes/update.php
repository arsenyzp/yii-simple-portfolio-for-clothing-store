<?php
$this->breadcrumbs=array(
	'Categori Sizes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CategoriSizes','url'=>array('index')),
	array('label'=>'Create CategoriSizes','url'=>array('create')),
	array('label'=>'View CategoriSizes','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage CategoriSizes','url'=>array('admin')),
);
?>

<h1>Update CategoriSizes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>