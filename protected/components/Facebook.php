<?php

class Facebook {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    const CLIENT_ID = '349623401834553';
    const CLIENT_SECRET = 'afd4de04e0b0945e7cccf31b00d1b896';
    const REDIRECT_PATH = '/index.php/facebook';

    public static  function Fauth(){
        if(isset($_GET['code'])){
            $code = $_GET['code'];
            $res = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=".self::CLIENT_ID."&redirect_uri=". Yii::app()->request->getBaseUrl(true) .self::REDIRECT_PATH."&client_secret=".self::CLIENT_SECRET."&code=".$code);
            $param = explode("=", $res);
            $tokken = $param[1];
            $res = file_get_contents("https://graph.facebook.com/me?access_token=".$tokken);
            $j = json_decode($res);
            $user = new Users();
            $user->login = $j->email;
            $user->setPasswd();
            $user->first_name = $j->first_name;
            $user->last_name = $j->last_name;
            $user->facebook_id = $j->id;
            $user->save();
            Yii::app()->session->add("id_us", $user->id);
            Yii::app()->session->add("regid", $user->first_name);
            return true;
        }
        return false;
    }

    public static  function getUrl(){
        return "https://www.facebook.com/dialog/oauth?client_id=".self::CLIENT_ID."&redirect_uri=". Yii::app()->request->getBaseUrl(true) .self::REDIRECT_PATH."&response_type=code&scope=email";
    }

}