<?php

class PreferenceProducts{

    public $limit = 18;
    public $offset = 0;

    private $rows = 3;
    private $subject = ": Your new Private Shopping arrivals are in";
    private $array_id;
    private $array_cat;
    private $array_size;
    private $products_id;

    /**
     * @param $userid
     * выбирает ид продуктов выбраных брендов
     */

    public  function getPreferenseBrand($userid){
        $connection=Yii::app()->db;
        $q = "SELECT `link_brands`.`product_id` as `t` FROM  `link_brands` WHERE `link_brands`.`brand_id` IN
       (SELECT `user_brands`.`brand_id` FROM `user_brands` WHERE `user_brands`.`user_id` = ".$userid.")";
        $dataReader = $connection->createCommand($q)->query();
        $rows_products_id = $dataReader->readAll();
        $this->array_id = array();
        foreach($rows_products_id as $prod_id){
            $this->array_id[] = $prod_id['t'];
        }

        //echo "Preference brand: ".count($this->array_id)."\n\r";
    }

    /**
    * @param $userid
     * выбирает ид продуктов выбраных категорий
    */
    public function getPreferenceCat($userid){
        $connection=Yii::app()->db;
        $q = "SELECT `user_categories`.`category_id` FROM `user_categories` WHERE `user_categories`.`user_id` = ".$userid;
        $dataReader = $connection->createCommand($q)->query();
        $rows_cat_id = $dataReader->readAll();
        $this->array_cat = array();
        foreach($rows_cat_id as $cat_id){
            $this->array_cat[] = $cat_id['category_id'];
        }

        //echo "Preference cat: ".count($this->array_cat)."\n\r";
    }

    /**
    * @param $userid
    * удаляет ид продуктов из ранее выбранных которые имеют размеры но не подходят под выбранные размеры
    * функция должна быть вызвана последней
    */
    public function getPreferenseSize($userid){
        $connection=Yii::app()->db;
/*
        $q = "SELECT `link_properties`.`product_id` FROM `link_properties` 
                    LEFT JOIN (`link_size` RIGHT JOIN `user_size` USING(`size_id`)) 
                        ON  `link_properties`.`prorerties_id` =  `link_size`.`properties_id`  
                            WHERE `user_size`.`user_id` = ".$userid;

*/
        $q = "SELECT `user_size`.`size_id` FROM `user_size` WHERE `user_size`.`user_id` = ".$userid;
        $dataReader = $connection->createCommand($q)->query();
        $rows_size_id = $dataReader->readAll();
        $t_size = array();
        foreach($rows_size_id as $size_id){
            $t_size[] = $size_id['size_id'];
        }

        if(count($t_size) != 0){
            $q = "SELECT `link_size`.`properties_id` FROM `link_size` WHERE `link_size`.`size_id` IN(".implode(',', $t_size).")";
            $dataReader = $connection->createCommand($q)->query();
            $rows_prop_id = $dataReader->readAll();
            $t_prop = array();
            foreach($rows_prop_id as $prop_id){
                $t_prop[] = $prop_id['properties_id'];
            }
        }


        //если запросы выше не выполнились то не учитываем размеры
        if(!isset($t_prop) || count($t_prop) == 0)
            $t_prop = array(0);

        //выбираем те которые имеют размер*************************/
        $q = "SELECT DISTINCT `link_properties`.`product_id` FROM `link_properties`";
        $dataReader = $connection->createCommand($q)->query();
        $rows_produts_id = $dataReader->readAll();

        $ttt = array();
        foreach($rows_produts_id as $product_id){
            $ttt[] = $product_id['product_id'];///////////////////////////////
        }
        /**********************/
        //исключаем из выбранных те которые имеют размер
        $res_tmp = array_unique(array_diff($this->array_id, $ttt));


        //получаем продукты которые подходят по размерам
        $q = "SELECT `link_properties`.`product_id` FROM `link_properties` WHERE `link_properties`.`prorerties_id` IN(".implode(',', $t_prop).")";

        $dataReader = $connection->createCommand($q)->query();
        $rows_produts_id = $dataReader->readAll();

        if(count($rows_produts_id) == 0)
            return;
        $this->array_size = array();
        foreach($rows_produts_id as $product_id){
            $this->array_size[] = $product_id['product_id'];///////////////////////////////
        }

        /*
         //если нет продуктов подходящих по размерам, то подбираем не учитывая размеры
        if(count($this->array_size) == 0)
            $result = array_unique($this->array_id);
        else
            $result = array_unique( array_intersect($this->array_id, $this->array_size));
            //$result = array_unique(array_diff($this->array_id, $this->array_size));
        if(count($result) == 0)
            $result = array_unique($this->array_id);
        */
        $result = array_unique(array_intersect($this->array_id, $this->array_size));
        //обьединяем те которые подошли по размерам и те которые не имеют размеров
        //$this->products_id = array_merge($result,  $res_tmp);
        $this->products_id = $result;

        //echo count($result);
    }

    /**
    * удаляет из выборки ранее продукты которые уже были отправлены пользователю
    */
    private function getSentProducts($userid){
        $sent_products = SentProducts::model()->findALLByAttributes(array('user_id'=>$userid));
        $tmp_sent = array();
        foreach ($sent_products as $sent_p) {
            /*
            if($k = array_search($sent_p->product_id, $this->products_id))
                unset($this->products_id[$k]);
               */
            $tmp_sent[] =  $sent_p->product_id;
        }
        $res = array();
        foreach ($this->products_id as $value) {
            if(! in_array($value, $tmp_sent)){
                $res[] = $value;
            }
        }

        $this->products_id = $res;
    }

    /*
    * формирует массив ид продуктов по выбранным настройкам
    *
    */
    private function preparationConditions($userid, $history = true, $size = true){
       // $this->getPreferenceCat($userid);
        $this->getPreferenseBrand($userid);

        //if($size)
          //  $this->getPreferenseSize($userid);
        $this->products_id = array_unique($this->array_id);
        if($history)
            $this->getSentProducts($userid);
    }

    /**
    * формирует часть письма с новыми продуктами
    *
    */
    public function getPreferenceProduct(){
    //для тестов выбор случайных продуктов без учёта настроек
     /*           
        return  Products::model()->findAllBySql("SELECT   `products` . * FROM `products`
                                                        ORDER BY RAND() LIMIT :limit", array(':limit'=>$this->limit));

    */
       if(count($this->products_id) == 0)
        return;
// AND `products`.`export_cat`  IN ('".implode("', '", $this->array_cat)."')
        return  Products::model()->findAllBySql("SELECT   `products` . * FROM `products`
                                                  WHERE  `products`.`id` IN ('".implode("', '",$this->products_id)."')
                                                   
                                                        AND (`products`.`was_price` = 0 OR `products`.`was_price` = `products`.`price`)
                                                            AND `products`.`status` = 'L'
                                                                ORDER BY `date_first_imported` DESC LIMIT :limit", array(':limit'=>$this->limit));
    }

    public function getPreferenceProductSale(){
    //для тестов выбор случайных продуктов без учёта настроек
    /*
                
         return  Products::model()->findAllBySql("SELECT   `products` . * FROM `products`
                                                           WHERE  `products`.`was_price` > 0
                                                                    ORDER BY RAND() LIMIT :limit", array(':limit'=>$this->limit));
        
    */
//AND `products`.`export_cat`  IN ('".implode("', '", array_unique($this->array_cat))."')
       if(count($this->products_id) == 0)
        return;

        return  Products::model()->findAllBySql("SELECT   `products` . * FROM `products`
                                                   WHERE  `products`.`id` IN ('".implode("', '",array_unique($this->products_id))."')
                                                  AND `products`.`was_price` - `products`.`price` >  `products`.`was_price` * 0.1 
                                                        AND `products`.`was_price` > 0
                                                            AND `products`.`was_price` > `products`.`price`
                                                                AND `products`.`status` = 'L'
                                                                    ORDER BY `date_first_imported` DESC LIMIT :limit", array(':limit'=>$this->limit));

    }

    /**
    * формирует часть запроса для выбора продуктов с определёнными скидками
    *
    */
    public function getPreferenceSale(){
        ///разобраться с запросами
        $preference_sale = Yii::app()->session->get('sale_preference');
        if(!is_array($preference_sale))
            return "";

        $sale_level = 0;
        foreach ($preference_sale as $key => $value) {
            if($value>0)
                $sale_level = $value;     
            }
        if($sale_level == 0)
             return "";   

    
//print_r($preference_sale);

        $sql = " AND (";
            if(isset($preference_sale[2]) && $preference_sale[2]>0){//<25%
                $sql .= " (`products`.`was_price` - `products`.`price` <  `products`.`was_price` * 0.25 AND `products`.`was_price` > 0 AND `products`.`was_price` > `products`.`price` )";
            }
            if(isset($preference_sale[3]) && $preference_sale[3]>0){//25-50%
                if(isset($preference_sale[2]) && $preference_sale[2]>0)
                    $sql .= " OR ";
                $sql .= " (`products`.`was_price` - `products`.`price` >  `products`.`was_price` * 0.25  AND  `products`.`was_price` - `products`.`price` <  `products`.`was_price` * 0.5 )";
            }
            if(isset($preference_sale[4]) && $preference_sale[4]>0){//50-75%
                if((isset($preference_sale[2]) && $preference_sale[2]>0) || (isset($preference_sale[3]) && $preference_sale[3]>0))
                    $sql .= " OR ";
                $sql .= "(`products`.`was_price` - `products`.`price` >  `products`.`was_price` * 0.5  AND  `products`.`was_price` - `products`.`price` <  `products`.`was_price` * 0.75 )";
            }
            if(isset($preference_sale[5]) && $preference_sale[5]>0){//>75%
               if((isset($preference_sale[2]) && $preference_sale[2]>0) || (isset($preference_sale[3]) && $preference_sale[3]>0) || (isset($preference_sale[4]) && $preference_sale[4]>0))
                    $sql .= " OR ";
                $sql .= "(`products`.`was_price` - `products`.`price` >  `products`.`was_price` * 0.75 )";
            }
        $sql .=") ";

        if(count($preference_sale) == 0 || (isset($preference_sale[0]) && $preference_sale[0]>0))//no sale
            $sql = "";
        if(isset($preference_sale[1]) && $preference_sale[1]>0){//Any sale
                $sql = " AND `products`.`was_price` > 0 AND `products`.`was_price` > `products`.`price` ";
            }

        return $sql;
    }

    /**
    * возвращает массив ид категорий выбраных пользователем (из сессии)
    */
    public function getTempCat(){
        $this->array_cat = Yii::app()->session->get("tcat_preference");
        if(count($this->array_cat) == 0){
            $arrayCategory = CategoriesPublick::model()->getListCategoriesPublick();
            $selected_category = array();
                foreach($arrayCategory as $c){
                    $selected_category[$c->term_id] = $c->term_id;
                }
            //Yii::app()->session->add("tcat_preference", $selected_category);
            $this->array_cat = $selected_category;
        }
    }

    /**
    * выбор продуктов в кабинете пользователя
    *
    */
    public function getPreferenceProductUser($user_id=0, $page=0){
       
        $limit = $this->limit; 
        $offset = $this->limit * ($page); 
        $this->preparationConditions($user_id, false);


        $this->getTempCat();
        $sale = $this->getPreferenceSale();
        
        // список продуктов для тестов
       /*
        $products = Products::model()->findAllBySql("SELECT   `products` . * FROM `products`
                                                        ORDER BY RAND() LIMIT :offset, :limit", array(':limit' => $limit, ':offset' => $offset));
        */  
                                        
       // if(count($this->products_id) == 0 || count($this->array_cat) == 0)
        /*   AND `products`.`export_cat`  IN ('".implode("', '", $this->array_cat)."')*/
        //print_r($this->array_id);
        //$this->products_id = $this->array_id;
        if(count($this->products_id) == 0)
            return;

        //print_r($this->array_cat);

        $products =  Products::model()->findAllBySql("SELECT   `products` . `brand_name`,
                                                                `products` . `product_name`,
                                                                `products` . `price`,
                                                                `products` . `was_price`,
                                                                `products` . `product_url`,
                                                                `products` . `image`,
                                                                `products` . `merchant_name`
                                                                 FROM `products`
                                                  WHERE  `products`.`id` IN ('".implode("', '",$this->products_id)."')
                                                    AND `products`.`export_cat`  IN ('".implode("', '", $this->array_cat)."')
                                                        AND `products`.`status` = 'L'
                                                            $sale
                                                                ORDER BY `date_first_imported` DESC  LIMIT :offset, :limit", array(':limit' => $limit, ':offset' => $offset));
                                                          

        if(!$products){
            //echo "Error select new products \n\r";
        }
        $count = count($products);
        $template_vars = array();
        foreach($products as $product){
            $res = array();
            $res['brand'] = $product->brand_name;
            $res['name'] = $this->replaceProductName($product->product_name, $product->brand_name);
            $res['price'] = $product->price;
            $res['was_price'] = $product->was_price;
            $res['is_sale'] = (Bool)($product->was_price > $product->price);
            //$res['link'] = $product->product_url;
            $res['link'] = $this->getAffileteUrlSite($product->product_url, $user_id);
            $res['img'] = $product->image;
            $res['merchant_name'] = $product->merchant_name;
            $template_vars[] = $res;
        }
        return $template_vars;
    }

    /**
    * удаляет из имени продукта название бренда
    *
    */
    public function  replaceProductName($name, $brand){
        return trim(str_replace($brand, '', $name));
    }

    /**
    * формирует ссылки на продукты (добавляет ид пользователя)
    *
    */
    public function getAffileteUrl($url, $userid){

        $uid = "EMAIL-PS-".$userid;
        // Affiliate Window
        if (strpos($url,'awin') !== false) {
            $url = str_replace("p=","clickref=$uid&p=",$url);
        }

        // Commission Junction
        if (strpos($url,'click-3824485') !== false) {
            $url = str_replace("?url","?sid=$uid&url",$url);
        }

        // Tradedoubler
        if (strpos($url,'tradedoubler') !== false) {
            $url = str_replace("&url","&epi=$uid&url",$url);
        }

        // Webgains
        if (strpos($url,'webgains') !== false) {
            $url = str_replace("&wgtarget","&clickref=$uid&wgtarget",$url);
        }
        // Linkshare
        if (strpos($url,'linksynergy') !== false) {
            $url = $url."&u1=".$uid;
        }

        return $url;
    }


     /**
    * формирует ссылки на продукты (добавляет ид пользователя) для кабинета
    *
    */
    public function getAffileteUrlSite($url, $userid){

        $uid = "Website-PS-".$userid;
        // Affiliate Window
        if (strpos($url,'awin') !== false) {
            $url = str_replace("p=","clickref=$uid&p=",$url);
        }

        // Commission Junction
        if (strpos($url,'click-3824485') !== false) {
            $url = str_replace("?url","?sid=$uid&url",$url);
        }

        // Tradedoubler
        if (strpos($url,'tradedoubler') !== false) {
            $url = str_replace("&url","&epi=$uid&url",$url);
        }

        // Webgains
        if (strpos($url,'webgains') !== false) {
            $url = str_replace("&wgtarget","&clickref=$uid&wgtarget",$url);
        }
        // Linkshare
        if (strpos($url,'linksynergy') !== false) {
            $url = $url."&u1=".$uid;
        }

        return $url;
    }

    /**
    * рендер файла шаблона
    *
    */
    public function renderFile($_viewFile_,$_data_=null,$_return_=false)
    {
        if(is_array($_data_))
            extract($_data_,EXTR_PREFIX_SAME,'data');
        else
            $data=$_data_;
        if($_return_)
        {
            ob_start();
            ob_implicit_flush(false);
            require($_viewFile_);
            return ob_get_clean();
        }
        else
            require($_viewFile_);
    }

    /**
    * Save sent products for user
    *
    */
    private function saveSentProduct($userid, $product_id){
        $p = new SentProducts;
        $p->user_id = $userid;
        $p->product_id = $product_id;
        $p->save();
    }

    /**
    *Send Email
    *
    */
    public  function sendEmailUser($userid, $user_email, $user_name){
        $this->preparationConditions($userid);
        $count = 0;
        $products = $this->getPreferenceProduct();
        if(!$products){echo "Error select new products \n\r";}
        $count = count($products);
        $template_vars = array();
        if($count >0)
        foreach($products as $product){
            $res = array();
            $res['brand'] = $product->brand_name;
            $res['name'] = $this->replaceProductName($product->product_name, $product->brand_name);
            $res['price'] = $product->price;
            //$res['link'] = $product->product_url;
            $res['link'] = $this->getAffileteUrl($product->product_url,$userid);
            $res['img'] = $product->image;
            $res['merchant_name'] = $product->merchant_name;
            $template_vars[] = $this->renderFile(Yii::getPathOfAlias('application.views.email.products').'.php' ,$res, true);
            $this->saveSentProduct($userid, $product->id);
        }

        $prod_template = array();
        $temp = "";
        foreach($template_vars as $k=>$v){
            $temp .= $v;
            if(($k+1)%$this->rows == 0){
                $prod_template[] = $this->renderFile(Yii::getPathOfAlias('application.views.email.row').'.php' , array('products'=>$temp), true);
                $temp = "";
            }
        }


        ////////////////////////////SALE///////////////////////////////////////

        $products = $this->getPreferenceProductSale();
        if(!$products){echo "Error select sale \n\r";}
        $count += count($products);
        $template_vars = array();
        if($count >0)
        foreach($products as $product){
            $res = array();
            $res['brand'] = $product->brand_name;
            //$res['name'] = $product->product_name;
            $res['name'] = $this->replaceProductName($product->product_name, $product->brand_name);
            $res['was_price'] = $product->was_price;
            $res['price'] = $product->price;
            $res['link'] = $this->getAffileteUrl($product->product_url,$userid);
            $res['img'] = $product->image;
            $res['merchant_name'] = $product->merchant_name;
            $template_vars[] = $this->renderFile(Yii::getPathOfAlias('application.views.email.producs2').'.php' ,$res, true);
            $this->saveSentProduct($userid, $product->id);
        }

        $prod_template2 = array();
        $temp = "";
        foreach($template_vars as $k=>$v){
            $temp .= $v;
            if(($k+1)%$this->rows == 0){
                $prod_template2[] = $this->renderFile(Yii::getPathOfAlias('application.views.email.row').'.php' , array('products'=>$temp), true);
                $temp = "";
            }
        }


        $email = Yii::app()->email;
        $email->to = $user_email;

        $email->from = 'MILAN STYLE <privateshopping@milanstyle.co.uk>';
        $email->subject = $user_name . $this->subject;

        $email->layout = "common";
        $email->view = 'main';

        $email->type = 'text/html';

        if($count > 0)
            if ($email->send(array(
                'user' => $user_name,
                'user_id' => $userid,
                'new' => implode(' ',$prod_template), 
                'sales'=>implode(' ',$prod_template2),
                'count_new'=>count($prod_template),
                'count_sale'=>count($prod_template2),
                )
                )) {
                echo "Email successfully send for  ".$user_email."\n\r\n\r";
            }
            else{
                echo "Error Send Message."."\n\r";
            }
        else{
            echo "No preference products for  ".$user_email."\n\r\n\r";
        }
    }
}